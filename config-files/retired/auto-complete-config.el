;;; Autocomplete configuration

;; auto-complete does not seems to work well with a new (emacs >= 26) display-line-numbers-mode.
;; The popups it shows are broken when display-line-numbers-mode is enabled.
;; We have to fix it manually.
(defun ac-update-display-line-numbers-mode-workaround (func &rest args)
  (if (not (bound-and-true-p display-line-numbers))
      (apply func args)
    (let ((old-display-line-numbers display-line-numbers))
      (unwind-protect
          (progn
            (setq display-line-numbers nil)
            (apply func args))
        (setq display-line-numbers old-display-line-numbers)))))

(defun ac-display-line-numbers-mode-workaround-enable ()
  (interactive)
  (advice-add 'ac-update :around 'ac-update-display-line-numbers-mode-workaround))

(defun ac-display-line-numbers-mode-workaround-disable ()
  (interactive)
  (advice-remove 'ac-update 'ac-update-display-line-numbers-mode-workaround))

(defun ac-mode-hook ()
  (setq-local ac-auto-start 1)
  (setq-local ac-auto-show-menu 0.2)
  ;; rebind standard completion keys
  (local-set-key (kbd "C-M-i") #'auto-complete)
  (unless (derived-mode-p 'prog-mode)
    (local-set-key (kbd "<tab>") #'auto-complete)))

(eval-after-load "auto-complete"
  '(progn
     (require 'pos-tip)
     ;; enable it globally
     (setq ac-quick-help-prefer-pos-tip t) ; to fix broken tooltip when using auto-complete mode

     ;;(auto-complete-mode t)
     ;; do not use Enter key for autocompletion
     (ac-display-line-numbers-mode-workaround-enable)
     (define-key ac-completing-map [return] nil)
     (define-key ac-completing-map "\r" nil)
     (add-hook 'auto-complete-mode-hook #'ac-mode-hook)))
;; (ac-config-default)

;;(eval-after-load "popup"
;;  '(setq popup-use-optimized-column-computation nil))
