(global-set-key (kbd "<f9>") 'sr-speedbar-toggle)

(with-eval-after-load 'sr-speedbar
  (setq sr-speedbar-right-side nil))

(with-eval-after-load 'speedbar
  (setq speedbar-tag-hierarchy-method '(speedbar-sort-tag-hierarchy)))
