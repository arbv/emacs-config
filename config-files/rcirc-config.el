;; rcirc configuration

(eval-and-compile
  (require 'rcirc))

;; this file should be used for authentication
;; it consist of associative list of variables and values
;; to set during configuration.
;; We will try to find this file in both ~/.emacs.d and ~/.emacs.data.
(defconst +rcirc-user-file+ "rcirc-user-data.el")

;;; Example user file contents:

;; (;; default user info
;;  (rcirc-default-nick . "jdoe")
;;  (rcirc-default-user-name . "John")
;;  (rcirc-default-full-name . "John Doe")
;;  ;; IRC servers (to use TLS you should have gnutls-cli installed)
;;  (rcirc-server-alist . (("irc.freenode.net"
;;						 :port 6697
;;						 :encryption tls
;;						 :channels ("#emacs"
;;									"##English"
;;									"#lisp"
;;									"#scheme"))))
;;  ;; authentication data
;;  (rcirc-authinfo . (("freenode" nickserv "jdoe" "p@ass")))
;;  )

;; utilities

(defun read-alist-from-file (file-path)
  (when (and file-path
			 (file-exists-p file-path))
	(let ((parse-sexp-ignore-comments t))
	  (read (get-string-from-file file-path)))))

(defun read-rcirc-auth-data ()
  (let ((file-path (cond
					((file-exists-p (concat user-emacs-directory
									+rcirc-user-file+))
					 (concat user-emacs-directory
							 +rcirc-user-file+))
					(t nil))))
	(read-alist-from-file file-path)))

(defun set-alist-variables (data)
  (mapc #'(lambda (e)
			(set (car e)
				 (cdr e))) data))

;; reread user data efore connection and joining channel
(defun rcirc-config-advice (func &rest args)
  (set-alist-variables (read-rcirc-auth-data))
  (apply func args))

;; kill buffer after you had parted the channel
(defun rcirc-part-advice (func &rest args)
  (cl-flet ((get-parted-buffer (process response channel sender nick args)
							   (rcirc-get-buffer process channel)))
	(let* ((buf (apply #'get-parted-buffer args))
		   (res (apply func args)))
	  (when (and buf (string= (rcirc-nick (nth 0 args))
							  (nth 4 args)))
		(kill-buffer buf))
	  res)))

(defun get-irc-server-buffers (&optional server-buffer)
  (unless server-buffer
	(setf server-buffer rcirc-server-buffer))
  (when server-buffer
	(cl-remove-if #'null
				  (mapcar #'(lambda (b)
							  (with-current-buffer b
								(when (and
									   (eq (buffer-mode b) 'rcirc-mode)
									   (eq rcirc-server-buffer server-buffer)
									   (not (eq b server-buffer)))
								  b)))
						  (buffer-list)))))

(defun get-irc-server-channel-list (&optional server-buffer)
  (let ((channel-buffers (get-irc-server-buffers server-buffer)))
	(cl-remove-if #'null
				  (mapcar #'(lambda (buf)
							  (let ((channel nil))
								(with-current-buffer buf
								  (if (and (bound-and-true-p rcirc-target)
										   (string-match-p "#" rcirc-target))
									  (setf channel rcirc-target)
									))))
						  channel-buffers))))

;; kill all channel buffers after you had quit the server
(defun rcirc-quit-advice (func &rest args)
  (let* ((buffers-to-kill (get-irc-server-buffers rcirc-server-buffer))
		 (server-buffer rcirc-server-buffer)
		 (res (apply func args))) ;; QUIT
	(mapc #'kill-buffer ;; kill all channel buffers
		  buffers-to-kill)
	;; delete irc process
	(with-current-buffer server-buffer
	  (let ((rcirc-reconnect-delay 0))
		(delete-process rcirc-process)))
	;; kill server buffer
	(kill-buffer server-buffer)
	res))

;; beep when your nick had been mentioned
(defun rcirc-on-my-nick-callback (process sender response target text)
  "This function called when your nick is mentioned in a channel buffer."
  (play-ding))

(defun call-my-nick-callback (process sender response target text)
  (when (and (string-match (regexp-quote (rcirc-nick process)) text)
			 (not (string= (rcirc-nick process) sender))
			 (not (string= (rcirc-server-name process) sender))
			 (rcirc-buffer-process)
			 (not (eq rcirc-server-buffer
					  (current-buffer))))
	(rcirc-on-my-nick-callback process sender response target text)))

;; configuration (based on: https://www.emacswiki.org/emacs/rcircExampleSettings)
(eval-after-load "rcirc"
  '(progn
	 (load-user-file "config-files/rcirc-custom-commands")
	 ;; set rcirc timeout
	 (setq rcirc-timeout-seconds 400)

	 ;; automatically reconnect after connection loss (starting from Emacs 25)
	 (when (>= emacs-major-version 25)
	   (setq-default rcirc-reconnect-delay 30))

	 ;; Include date in time stamp.
	 (setq rcirc-time-format "%d.%m.%y %H:%M ")

	 ;; set automatically generated anonymous user name as default
	 (let ((tmp-user-name (format "Rcirc%d" (random 65536))))
	   (setq rcirc-default-nick tmp-user-name
			 rcirc-default-user-name tmp-user-name
			 rcirc-default-full-name tmp-user-name))

	 ;; automatic authorisation
	 (setq rcirc-auto-authenticate-flag t)

	 (set-alist-variables (read-rcirc-auth-data))

	 (advice-add 'rcirc :around #'rcirc-config-advice)
	 (advice-add 'rcirc-cmd-join :around #'rcirc-config-advice)
	 (advice-add 'rcirc-cmd-quit :around #'rcirc-quit-advice)
	 (advice-add 'rcirc-handler-PART-or-KICK :around #'rcirc-part-advice)

	 (add-hook 'rcirc-print-functions 'call-my-nick-callback)

	 ;; Scroll conservatively
	 ;; Keep input line at bottom.
	 (add-hook 'rcirc-mode-hook
			   (lambda ()
				 (set (make-local-variable 'scroll-conservatively)
					  8192)))

	 ;; enable omit mode by default
	 (add-hook 'rcirc-mode-hook
			   (lambda ()
				 (rcirc-omit-mode)))

	 ;; track mode
	 (add-hook 'rcirc-mode-hook
			   (lambda ()
				 (rcirc-track-minor-mode 1)))

	 ;; enable spell checking
	 (add-hook 'rcirc-mode-hook
			   (lambda ()
				 (flyspell-mode 1)))

	 ;; automatically disable rcirc-track-minor-mode when no rcirc buffers left
	 (add-hook 'kill-buffer-hook
			   (lambda ()
				 (let* ((buf (current-buffer))
						(result (cl-reduce #'(lambda (a b)
											   (or a b))
										   (mapcar #'(lambda (e)
													   (unless (and
																(eq (buffer-mode buf) 'rcirc-mode)
																(eq buf e))
														 (eq (buffer-mode e) 'rcirc-mode)))
												   (buffer-list)))))
				   (unless result
					 (rcirc-track-minor-mode -1)))))

	 ;; automatically send QUIT command on killing RCIRC process buffer
	 (add-hook 'kill-buffer-hook
			   (lambda ()
				 (with-current-buffer (current-buffer)
				   (when (and rcirc-process
							  (process-live-p rcirc-process))
					 (rcirc-cmd-quit nil rcirc-process rcirc-target)))))

	 ))
