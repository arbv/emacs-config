(with-eval-after-load 'eglot
  (when (executable-find "clangd")
    (let ((clangd-options '("--completion-style=detailed" "--header-insertion=never")))
      (add-to-list 'eglot-server-programs `((c++-mode c-mode) "clangd" ,@clangd-options))
      (add-to-list 'eglot-server-programs `((objc-mode) "clangd" ,@clangd-options)))))
