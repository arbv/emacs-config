;; redefine lua-mode internal function to handle REPL requests longer than 500 characters
(defun lua-send-string (chunk-str)
  "Load CHUNK-STR plus a newline into Lua subprocess.

If `lua-process' is nil or dead, start a new process first."
  (let ((str (if (string-equal (substring chunk-str -1) "\n")
                 chunk-str
               (concat chunk-str "\n")))
        (lua-process (lua-get-create-process)))
    (if (<= (length str) 500) ; handle "short" strings directly
        (process-send-string lua-process str)
      (let ((tmp-file (make-temp-file "luamode" nil ".tmp"))) ; handle long strings via temporary files
        ;; write data into temporary file
        (with-temp-buffer
          (insert str)
          (write-file tmp-file))
        ;; evaluate data in the temporary file and then remove it
        (process-send-string
         lua-process
         (format (concat
                  "\n"
                  "local tmp = '%s';"
                  "local res, e = pcall(function () "
                  "  local do_loadstring = loadstring or load;"
                  ""
                  "  local f, e = io.open(tmp, 'r');" ; open temporary file
                  "  if e then "
                  "    os.remove(tmp);"
                  "    error(e);"
                  "    return;"
                  "  end "
                  ""
                  "  local cont, e = f:read('*all');" ; read all data
                  "  if e then "
                  "    os.remove(tmp);"
                  "    error(e);"
                  "    return;"
                  "  end "
                  ""
                  "  f:close(); f = nil;" ; close and remove file
                  "  os.remove(tmp);"
                  ""
                  "  local f, e = do_loadstring(cont);" ; handle chunk
                  "  if e then "
                  "    error(e);"
                  "    return;"
                  "  end "
                  ""
                  "  return f();" ; execute chunk
                  "end);"
                  "if e then _, _ = os.remove(tmp); error(e); end" ; handle error, if any
                  "\n")
                 tmp-file))))))

