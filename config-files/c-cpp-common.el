;;;; common configuration for C/C++
(defun get-c-cpp-compile-command (&optional directory target)
  (concat
   (if (and (system-is-windows)
			(executable-exists-p "mingw32-make"))
	   "mingw32-make"
	 "make")
   " -k"
   (format " -j%d" (ncpu))
   (if (not directory)
	   (concat
		" -f "
		"\""
		(emacs-path-to-system-path (concat (expand-file-name user-emacs-directory)
										   "misc/mkskel/GNUmakefile"))
		"\""
		" "
		(or target
			"debug"))
	 (concat
	  " -s -C "
	  "\""
	  (emacs-path-to-system-path (expand-file-name (directory-file-name directory)))
	  "\""))))

(defun c-cpp-compile-hook ()
  (let ((buffer-file-name (buffer-file-name)))
	(when buffer-file-name
	  (let ((makefile-dir (or (locate-dominating-file (buffer-file-name) "Makefile")
							  (locate-dominating-file (buffer-file-name) "GNUmakefile"))))
		(let ((compile-command (get-c-cpp-compile-command makefile-dir)))
		  (when compile-command
			(set (make-local-variable 'compile-command)
				 compile-command)))))))

;; C common mode setup
(defun c-common-mode-setup ()
  (c-set-offset 'case-label '+))

(with-eval-after-load 'cc-mode
  ;; BSD/Allman indentation style
  (setq-default c-default-style "bsd"
				c-basic-offset 4)

  (add-hook 'c-mode-common-hook 'c-common-mode-setup)

  ;; clang format
  (add-hook 'c-mode-common-hook
			(lambda ()
			  (when (derived-mode-p 'c-mode 'c++-mode)
				(clang-format-enable-if-available))))

  (add-hook 'c-mode-common-hook
			(lambda ()
			  (when (derived-mode-p 'c-mode 'c++-mode 'asm-mode)
				(c-cpp-compile-hook))))
  ;; enable cscope minor mode (from the xscope library)
  ;;(cscope-setup))
  ;; enable ggtags for C-derived modes
  (add-hook 'c-mode-common-hook
		    (lambda ()
			  (when (derived-mode-p 'java-mode 'asm-mode)
			    (ggtags-mode 1)))))

(defun cc-mode-setup ()
  (cond
   ((get-arduino-sketch-name)
	(enable-irony-and-ggtags))
   (t (when (executable-find "clangd")
		(eglot-common-setup)))))

(with-eval-after-load 'cc-mode
  ;; enable Irony for supported C-like languages.
  (add-hook 'c++-mode-hook 'cc-mode-setup)
  (add-hook 'c-mode-hook 'cc-mode-setup)
  (add-hook 'objc-mode-hook 'cc-mode-setup))
