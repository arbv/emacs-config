;; configuration for lua mode

(eval-after-load "lua-mode"
  '(progn
	 (load-user-file "config-files/lua-mode-fix")
	 ;; set indentation level into two spaces
	 (setq lua-indent-level 2)

	 ;; disable using of spaces during indentation
	 (add-hook 'lua-mode-hook (lambda ()
								(setq-local indent-tabs-mode nil)))

	 (add-hook 'lua-mode-hook 'company-mode)

	 ;; enable syntax checking
	 (add-hook 'lua-mode-hook 'flymake-lua-load)

	 ;; key bindings
	 (add-hook 'lua-mode-hook (lambda ()
								(local-set-key (kbd "C-c C-c") 'lua-send-proc) ;; send function to the interpreter
								(local-set-key (kbd "C-c C-r") 'lua-send-region) ;; send current region to the interpreter
								))
	 ))

(eval-after-load "company"
  '(progn
	 ;; enable auto completion for Lua
	 (add-to-list 'company-backends 'company-lua)))
