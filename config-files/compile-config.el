(defvar *compilation-window-close-timeout* 2
  "Compilation window close timeout on successful compilation. 0 - do not close. <0 - close immediately.")

;; limit number of lines in compilation buffer
(with-eval-after-load 'comint
  (setq comint-buffer-maximum-size 1024)
  (add-hook 'comint-output-filter-functions
		    'comint-truncate-buffer))

;; automatically close compilation window on successful compilation
(defun auto-close-compilation-window (status code message)
  "Automatically close the compilation windows if there was no errors or warnings."
  (let ((buffer (get-buffer "*compilation*"))
		(result (cons message (format "%d" code))))
	(when (and (or (eq status 'finished)
				   (eq status 'exit))
			   (zerop code)
			   (not
				(with-current-buffer buffer
				  (search-forward "warning" nil t))))
	  (unless (zerop *compilation-window-close-timeout*)
		(run-with-timer *compilation-window-close-timeout* nil
						(lambda (buf)
						  (let* ((compilation-buffer (get-buffer "*compilation*"))
								 (window (and compilation-buffer (get-buffer-window compilation-buffer)))
								 (current-buffer (current-buffer)))
							(when window
							  (quit-window nil window))
							(when (and current-buffer
									   (get-buffer-window current-buffer))
							  (select-window (get-buffer-window current-buffer)))))
						buffer)))
	result))

(defvar *last-compile-project-root* nil)
(defvar *last-compile-command* nil)

;; We use advice in order to reuse the last used compile command for
;; the other files in a project.
(defun compile-advice (func &rest args)
  (cl-labels ((current-project-root ()
                                    (let ((proj (project-current nil default-directory)))
                                      (when proj
                                        (project-root proj)))))
    (let* ((%current-root (current-project-root))
           (compile-command (if (and *last-compile-project-root*
                                     %current-root
                                     (string-equal *last-compile-project-root*
                                                   %current-root))
                                *last-compile-command*
                              compile-command)))
      (unwind-protect
          (apply func args)
        (setq *last-compile-command* compile-command
              *last-compile-project-root* %current-root
			  compilation-directory %current-root)))))

;; limit number of lines in compilation buffer
(with-eval-after-load 'compile
  ;(setq compilation-scroll-output 'first-error)
  (setq compilation-scroll-output t)
  (setq compilation-exit-message-function 'auto-close-compilation-window)
  (advice-add 'compile :around #'compile-advice)
  (advice-add 'recompile :around #'compile-advice))

;; define some keys to call compilation
(global-set-key (kbd "<f5>") 'recompile)
(global-set-key (kbd "<S-f5>") 'compile)
