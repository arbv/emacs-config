;;; Rust programming language support ;;;
;; This configuration supports only RLS LSP server

;;; Common Functionality

(defun rust-browse-documentation ()
  (interactive)
  (browse-url
   (concat "https://static.rust-lang.org/doc/master/std/index.html?search="
		   (thing-at-point 'symbol))))

(defun rust-setup-browse-documentation ()
  (local-set-key (kbd "C-c d") #'rust-browse-documentation)
  (local-set-key (kbd "C-c C-d") #'rust-browse-documentation))

;; set cargo home directory on Windows
(defun update-cargo-home ()
  (when (system-is-windows)
	(unless (getenv "CARGO_HOME")
	  (setenv "CARGO_HOME" (concat
							(get-home-directory)
							(file-name-as-directory ".cargo"))))))

(defun return-rust-compilation-command ()
  "Return compilation command string."
  (if (locate-dominating-file (buffer-file-name) "Cargo.toml")
	  "cargo build"
	(format "rustc %s && %s" (buffer-file-name)
			(file-name-sans-extension (buffer-file-name)))))

(defun rust-compile-hook ()
  (set (make-local-variable 'compile-command)
	   (return-rust-compilation-command)))

;;; Eglot configuration

(with-eval-after-load 'eglot
  (when (executable-find "rls")
	(add-to-list 'eglot-server-programs '((rust-mode) "rls"))))

(defun rust-eglot-setup ()
  (setq-local indent-tabs-mode nil)
  (update-cargo-home)
  ;; enable cargo minor mode
  (when (executable-exists-p "cargo")
	(cargo-minor-mode))
  ;; set documentation browsing keys
  (rust-setup-browse-documentation)
  ;; decide on Eglot (via RLS) or Racer
  (when (and (executable-find "rls")
			 (locate-dominating-file (buffer-file-name) "Cargo.toml"))
	(eglot-common-setup)))

(with-eval-after-load "rust-mode"
  ;; auto-completion
  (add-hook 'rust-mode-hook #'rust-eglot-setup)
  (add-hook 'rust-mode-hook  #'rust-compile-hook))
