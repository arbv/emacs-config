;;Taken from http://www.emacswiki.org/emacs/rcircReconnect and tweaked.
(defun-rcirc-command reconnect (arg)
  "Reconnect the server process."
  (interactive "i")
  (when (buffer-live-p rcirc-server-buffer)
	(with-current-buffer rcirc-server-buffer
	  (setq process rcirc-process)))
  (unless process
	(error "There's no process for this target"))
  (unless (process-live-p process) ; reconnect only when IRC process is dead
	(if (buffer-live-p rcirc-server-buffer)
		(with-current-buffer rcirc-server-buffer
		  (let ((reconnect-buffer (current-buffer))
				(server rcirc-server)
				(port (if (boundp 'rcirc-port)
						  rcirc-port
						rcirc-default-port))
				(nick (or rcirc-nick
						  rcirc-default-nick))
				channels
				encryption
				password)
			(dolist (buf (buffer-list))
			  (with-current-buffer buf
				(when (eq reconnect-buffer rcirc-server-buffer)
				  (remove-hook 'change-major-mode-hook
							   'rcirc-change-major-mode-hook)
				  ;; get default channels
				  (let ((server-plist (cdr (assoc-string server rcirc-server-alist))))
					(when server-plist
					  (setq channels (plist-get server-plist :channels))
					  (let ((server-port (plist-get server-plist :port))
							(server-encryption (plist-get server-plist :encryption))
							(server-password (plist-get server-plist :password)))
						(when server-port
						  (setq port server-port))
						(when server-encryption
						  (setq encryption server-encryption))
						(when server-password
						  (setq password server-password))
					  )))
				  ;; find already connected channels
				  (let ((channels-list (get-irc-server-channel-list rcirc-server-buffer)))
					(when channels-list
					  (setq channels channels-list)))
				  )))
			(delete-process process)
			;; reset rcirc tracking modestring
			(when (bound-and-true-p rcirc-activity-string)
			  (setq rcirc-activity-string ""))
			(rcirc-connect server port nick
						   nil
						   nil
						   channels
						   password
						   encryption)
			)))))

;; RCIRC reconnect Emacs command
(defun rcirc-reconnect ()
  (interactive)
  (if rcirc-server-buffer
	  (with-current-buffer rcirc-server-buffer
		(switch-to-buffer rcirc-server-buffer)
		(rcirc-cmd-reconnect nil rcirc-process rcirc-target))
	(error "There is no IRC server associated with this buffer.")))

(defun %rcirc-try-reconnect (process sentinel)
  (when (and (string-match-p "broken by remote peer" sentinel)
			 (not (string= sentinel "deleted")) ; to avoid conflicts with original RCIRC code
			 (< 0 rcirc-reconnect-delay)
			 (not (process-live-p process)))
	(message "Trying reconnect. Sentinel: %s" sentinel)
	(let ((now (current-time)))
	  (when (or (null rcirc-last-connect-time)
				(< rcirc-reconnect-delay
				   (float-time (time-subtract now rcirc-last-connect-time))))
		(setq rcirc-last-connect-time now)
		(rcirc-cmd-reconnect nil)))))

;;(cl-pushnew #'%rcirc-try-reconnect rcirc-sentinel-functions)

(defalias 'irc-reconnect #'rcirc-reconnect)

