(defun flymake-local-keybindings ()
  ;; go to next/previous flycheck error error
  (local-set-key (kbd "M-n") #'flymake-goto-next-error)
  (local-set-key (kbd "M-p") #'flymake-goto-prev-error))

(eval-after-load 'flymake
  '(add-hook 'flymake-mode-hook #'flymake-local-keybindings))
