;;; !!! Local Emacs TeXinfo documentation setup !!!
(eval-when-compile (require 'cl))

;; You could specify path to the your directory with info files
;; in EMACS_INFO_PATH environmental variable.
;; The ".emacs.d/info" will be used otherwise.

;; Local emacs INFO directory
(defvar *user-info-directory* (let ((info-path (getenv "EMACS_INFO_PATH")))
								(cond
								 ((and info-path
									   (directory-exists-p info-path))
								  (file-name-as-directory (system-path-to-emacs-path info-path)))
								 (t (expand-file-name (file-name-as-directory (concat user-emacs-directory "info")))))))

(defun extract-info-dir-entry (file-path)
  "Extract TexInfo directory entry from info file."
  (let ((contents (read-lines file-path)))
	(when contents
	  (let ((dir-entry-start (cl-member-if #'(lambda (e)
											   (string-match "START-INFO-DIR-ENTRY"
															 e))
										   contents)))
		(when dir-entry-start
		  (cl-find-if #'(lambda (e)
						  (plusp (length e)))
					  (cdr dir-entry-start)))))))

(defun find-info-files-in-directory (directory-path)
  "Find TeXinfo files in the given directory."
  (let ((files (directory-files (file-name-as-directory directory-path))))
	(cl-remove-if-not #'(lambda (e)
						  (string-match ".info$"
										(downcase e)))
					  files)))

;; (defun info-dir-up-to-date-p (directory-path)
;;   (let ((files (find-info-files-in-directory directory-path))
;; 		(dir-file 			(concat (file-name-as-directory directory-path)
;; 									"dir")))
;; 	(cond
;; 	 (files
;; 	  (and (file-exists-p dir-file)
;; 		   (every #'(lambda (file)
;; 					  (file-newer-than-file-p dir-file
;; 											  file))
;; 				  (mapcar #'(lambda (fname)
;; 							  (concat (file-name-as-directory directory-path)
;; 									  fname))
;; 						  files))))
;; 	 (t nil))))

(defun get-info-entries-in-directory (directory-path)
  "Extract directory entries from TeXinfo files in the given directory."
  (let ((dir-path (file-name-as-directory directory-path)))
	(when (directory-exists-p dir-path)
	  (let ((entries (mapcar #'extract-info-dir-entry
							 (mapcar #'(lambda (file-name)
										 (concat dir-path
												 file-name))
									 (find-info-files-in-directory dir-path)))))
		(sort entries #'(lambda (a b)
						  (string< (downcase a)
								   (downcase b))))))))

(defun write-info-directory-file (file-path &optional entries)
  "Update TeXinfo directory file with given entries."
  (let ((old-contents (when (file-exists-p file-path)
						(read-lines file-path))))
	(when entries
	  ;; generate dir file header
	  (with-temp-buffer
		(insert-char #x1f)
		(newline)
		(insert "File: dir,	Node: Top")
		(newline)
		(newline)
		(insert "* Menu:")
		(newline)
		(newline)
		(insert "User local documentation")
		(newline)
		(mapc #'(lambda (entry)
				  (insert entry)
				  (newline))
			  entries)
		(newline)
		;; Compare contents of the old file with the new generated one.
		;; We should write new file only when there is some differences. 
		(when (or (null old-contents)
				  (not (cl-equalp
						old-contents
						(split-string (buffer-string) "\n" t))))
		  (when (file-exists-p file-path) ; delete old file if we need to
			(delete-file file-path))
		  (append-to-file nil nil file-path)
		  t)))))

(defun update-info-entries-in-directory (directory-path)
  (let ((directory-path (file-name-as-directory directory-path)))
	(when (directory-exists-p directory-path)
	  (write-info-directory-file (concat directory-path
										 "dir")
								 (get-info-entries-in-directory
								  directory-path)))))

(defun update-user-info-entries ()
  "Update TeXinfo entries in well known to Emacs locations."
  (interactive)
  (update-info-entries-in-directory *user-info-directory*))


;; automatically update TeXinfo entries information.
(defun info-update-local-advice (func &rest args)
  ;; add directory if it exists
(when (directory-exists-p *user-info-directory*)
  (add-to-list 'Info-directory-list *user-info-directory* t))
  (update-user-info-entries)
  (apply func args))

(advice-add 'info :around #'info-update-local-advice)

