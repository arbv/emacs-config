(with-eval-after-load 'eglot
  (require 'eglot-fsharp))

(with-eval-after-load 'eglot-fsharp
  (when (fboundp 'eglot-fsharp--maybe-install)
    (eglot-fsharp--maybe-install)))

(defun arbv/fsharp-mode-config ()
  (when (executable-find "dotnet")
    (setq-local compile-command "dotnet build -c Debug")
    (eglot-common-setup)))

(with-eval-after-load 'inf-fsharp-mode
  (when (executable-find "dotnet")
    (unless (executable-find "fsharpi")
      (setq inferior-fsharp-program "dotnet fsi --readline"))))

(with-eval-after-load 'fsharp-mode
  (add-hook 'fsharp-mode-hook 'arbv/fsharp-mode-config))
