;;; Python IDE with eglot

(defun arbv/python-mode-setup ()
  (when (executable-exists-p "pyls")
    (eglot-common-setup)))

(with-eval-after-load 'python
  (add-hook 'python-mode-hook 'arbv/python-mode-setup))
