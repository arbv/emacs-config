;; Based on a suggestion from:
;; https://www.reddit.com/r/emacs/comments/7uq9w1/replace_emacs_c_autoformatting_with_clangformat/

(defconst +clang-format-default-filename+ ".clang-format")
(defconst +clang-format-headers-filename+ ".clang-format.headers")

(defvar *clang-format-enabled-local* nil)
(defvar *clang-format-assume-filename* +clang-format-default-filename+)

(defun clang-format-local-indent ()
  (interactive)
  (let ((reg-begin (if (use-region-p)
                       (region-beginning)
                     (point)))
        (reg-end (if (use-region-p)
                     (region-end)
                   (point))))
    (clang-format-region reg-begin reg-end nil *clang-format-assume-filename*)))

(defun clang-format-enable-local ()
  (setq-local *clang-format-enabled-local* t)
  (local-set-key (kbd "C-i") #'clang-format-local-indent)
  (local-set-key (kbd "C-M-\\") #'clang-format-local-indent))

(defun clang-format-enable-if-available ()
  (let ((bfn (buffer-file-name)))
    (when (and bfn
               (executable-find "clang-format")
               (locate-dominating-file bfn  +clang-format-default-filename+))
      (let ((ext (file-name-extension (buffer-file-name))))
        (when (and ext
                   (string= (char-to-string (car (string-to-list ext)))
                            "h")
                   (locate-dominating-file bfn  +clang-format-headers-filename+))
          (setq-local *clang-format-assume-filename*  +clang-format-headers-filename+)))
      (clang-format-enable-local))))

(defun c-indent-region-clang-format-advice (func &rest args)
  (if (bound-and-true-p *clang-format-enabled-local*)
      (apply #'clang-format-region args)
    (apply func args)))

(with-eval-after-load 'cc-mode
  (advice-add 'c-indent-region :around #'c-indent-region-clang-format-advice))

(defun clang-format-buffer-on-save ()
  (interactive)
  (when (bound-and-true-p *clang-format-enabled-local*)
    (clang-format-region (point-min) (point-max) nil *clang-format-assume-filename*)))

(with-eval-after-load 'files
  (add-hook 'before-save-hook 'clang-format-buffer-on-save))
