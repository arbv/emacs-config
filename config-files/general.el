;;;; !!! General Setup !!!

;;; Windows shell and sub-processes codepage setup
(defconst +windows-shell-codepage+ 'cp1251-dos) ; codepage for shell itself
(defconst +windows-filesystem-codepage+ 'cp1251-dos) ; filesystem codepage on Windows
(defconst +windows-subprocess-codepage+ 'cp866-dos)       ; codepage for sub-processes
(defvar   *input-methods* '(russian-computer ukrainian-computer)) ; input methods to switch by C-\


;; list of preferred coding system (typical for East slavic countries)
(defvar *preferred-coding-systems* '(utf-8
									 utf-8-mac
									 utf-8-dos
									 utf-8-unix
;;									 utf-8-hfs
;;									 utf-8-hfs-mac
;;									 utf-8-hfs-dos
;;									 utf-8-hfs-unix
									 cp1251
									 cp1251-mac
									 cp1251-dos
									 cp1251-unix
									 cp866
									 cp866-mac
									 cp866-dos
									 cp866-unix
									 cp866u
									 cp866u-mac
									 cp866u-dos
									 cp866u-unix
									 utf-16-le
									 utf-16-le-mac
									 utf-16-le-dos
									 utf-16-le-unix
									 utf-16-be
									 utf-16-be-mac
									 utf-16-be-dos
									 utf-16-be-unix
									 koi8-r
									 koi8-r-mac
									 koi8-r-dos
									 koi8-r-unix
									 koi8-u
									 koi8-u-mac
									 koi8-u-dos
									 koi8-u-unix))

;; Unicode language environment
(set-language-environment "UTF-8")
;;(add-to-list 'after-init-hook #'(lambda ()
;;								  (mapc #'prefer-coding-system (reverse *preferred-coding-systems*))))

;; set file coding system on Windows
(when (system-is-windows)
  (set-file-name-coding-system +windows-filesystem-codepage+))
;; do not create backup files
(setq make-backup-files nil)
;; turn on syntax highlighting
(turn-on-font-lock)
(global-font-lock-mode 1)
(setq font-lock-maximum-decoration t)
;; share clipboard with OS
(if (>= emacs-major-version 25)
	(setq select-enable-clipboard t)
  (setq x-select-enable-clipboard t))
;; shorter answers
(defalias 'yes-or-no-p 'y-or-n-p)
;; disable splash screen
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
;; blink cursor mode
(blink-cursor-mode t)
;; blink forever
(setq blink-cursor-blinks 0)
;; reaload file if changed on disc
(global-auto-revert-mode t)
;; silently ignore unsafe (or unknown) local variables
(setq enable-local-variables :safe)
;; always select new help window
(setq help-window-select t)

;; save mini-buffer history file
(eval-after-load "savehist"
  '(when (emacs-server-process-p)
	 (setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))))
(savehist-mode 1)

;; change pipe read delay on Windows
(when (system-is-windows)
  (setq w32-pipe-read-delay 0))

;; change switch to other window combination
(global-set-key (kbd "C-'") 'other-window)

(defun Buffer-menu-other-window-with-menu-closing ()
  "Select this line's buffer in other window, closing buffer menu."
  (interactive)
  (let ((window (get-buffer-window (current-buffer))))
	(switch-to-buffer-other-window (Buffer-menu-buffer t) t)
	(quit-window nil window)))

;; automatically switch to the new "*Buffer List*" window
(defun list-buffers-other-window (&optional arg)
  "List all buffers and switch to the *Buffer List* window.~
Works like LIST-BUFFERS but switches to the new window.
Closes buffer menu after window selection."
  (interactive "P")
  (let ((old-buffer (current-buffer)))
	(let* ((buffer-list (list-buffers-noselect arg)))
	  (display-buffer buffer-list)
	  (with-current-buffer buffer-list
		(let ((map-copy (copy-keymap (copy-keymap Buffer-menu-mode-map))))
		  (use-local-map map-copy)
		  (local-set-key (kbd "RET") 'Buffer-menu-other-window-with-menu-closing)))
	  (select-window (get-buffer-window buffer-list))
	  (list-buffers--refresh nil old-buffer))))

(global-set-key "\C-x\C-b" 'list-buffers-other-window)
;;(global-set-key "\C-x\C-b" 'buffer-menu-other-window)

;; move auto save directory
(defconst +auto-save-directory+  (concat user-emacs-directory
										 (file-name-as-directory "auto-save-list")))

(unless (file-exists-p +auto-save-directory+)
  (make-directory +auto-save-directory+ t))

(setq auto-save-list-file-prefix (concat +auto-save-directory+ ".saves-"))
(setq auto-save-file-name-transforms `((".*" ,+auto-save-directory+ t)))

(setq backup-by-copying t)
(setq backup-directory-alist `((".*" . ,+auto-save-directory+)
							   (,tramp-file-name-regexp nil)))


;; Show-paren-mode settings
(show-paren-mode t)
;(setq show-paren-style 'expression)

;; highlight search results
(setq search-highlight t)
(setq query-replace-highlight t)

;; TAB and indenting
(setq-default tab-width 4)
(setq-default c-basic-offset 4)
;; disable indentation with tabs
(setq-default indent-tabs-mode nil)
;; indent with tabs if file already contains TABs
(add-hook 'find-file-hook (lambda()
							(when (buffer-contains-substring "\t")
							  (setq-local indent-tabs-mode t))))

;; Easy transition between buffers: M-arrow-keys
(if (equal nil (equal major-mode 'org-mode))
	(windmove-default-keybindings 'meta))
;; C-tab switches to a next window
(global-set-key [(control tab)] 'other-window)

;; smooth scrolling
(setq scroll-margin 1
	  scroll-conservatively 0
	  scroll-up-aggressively 0.01
	  scroll-down-aggressively 0.01
	  ;;scroll-step 3
	  mouse-wheel-progressive-speed nil)
(setq-default scroll-up-aggressively 0.01
			  scroll-down-aggressively 0.01)
;; scroll 3 lines a time
(setq mouse-wheel-scroll-amount '(3 ((shift) . t) ((control) . nil)))

;; automatically close empty *Compile-Log* after initialization
(add-hook 'after-init-hook (lambda ()
							 (let ((compile-log (get-buffer "*Compile-Log*")))
							   (when compile-log
								 (kill-buffer compile-log)))))

(when (system-is-windows)
  (setq default-process-coding-system (cons +windows-subprocess-codepage+ +windows-subprocess-codepage+))
  (defadvice shell (after my-shell-advice)
	(set-buffer-process-coding-system +windows-shell-codepage+ +windows-shell-codepage+))
  (ad-activate 'shell))

;; line numbers
(eval-after-load "linum"
  '(setq linum-format "%3d"))
(if (< emacs-major-version 26)
	(progn
	  (line-number-mode t)
	  ;; linum mode only on buffers associated with files
	  (add-hook 'find-file-hook #'linum-mode))
  (progn
	;; use native line numbers mode on Emacs 26 and newer
	(add-hook 'find-file-hook #'(lambda ()
								  (when (bound-and-true-p linum-mode) ;; disable linum mode
									(linum-mode))
								  (display-line-numbers-mode t)))))
;; enable column numbers
(column-number-mode t)

;; highlight current line in some modes
;;(global-hl-line-mode t)
(defun enable-hl-line-mode ()
  (hl-line-mode t)
  ;; disable underlining of the highlighted line
  (set-face-attribute hl-line-face nil :underline nil))

;; (add-hook 'prog-mode-hook (lambda  ()
;;							(when (is-gui-running)
;;							  (enable-hl-line-mode))))
(add-hook 'dired-mode-hook #'enable-hl-line-mode)
(add-hook 'Buffer-menu-mode-hook #'enable-hl-line-mode)

;; (defun whitespace-prog-cleanup-on-save ()
;;   (when (derived-mode-p 'prog-mode)
;;	(whitespace-cleanup)))

;; (add-hook 'before-save-hook #'whitespace-cleanup)
(when (fboundp 'global-ethan-wspace-mode)
  (global-ethan-wspace-mode 1))
(with-eval-after-load 'ethan-wspace
  (setq mode-require-final-newline nil)
  (set-default 'ethan-wspace-errors (remove 'tabs ethan-wspace-errors)))

;;; IDO mode (interactively do things)
(eval-after-load "ido"
  '(progn
	 (setq ido-virtual-buffers t)
	 (setq ido-enable-flex-matching t)
	 ;; disable IDO in some places
	 (disable-ido-advice 'dired-create-directory)
	 (disable-ido-advice 'dired-do-symlink)
	 (disable-ido-advice 'dired-do-hardlink)))
(ido-mode t)
;;(icomplete-mode t)
(ido-everywhere t)

;;; Winner mode - switch between window configurations
(winner-mode t)

;; layout switching
(defun switch-to-next-input-method ()
  (interactive)
  (cond
   (default-input-method
	 (let ((next-list (member default-input-method *input-methods*)))
	   (if (and next-list
				(cadr next-list))
		   (set-input-method (cadr next-list) t)
		 (set-input-method nil t))))
   (t (when *input-methods*
		(set-input-method (car *input-methods*) t)))))

(setq default-input-method nil)
(global-set-key "\C-\\" 'switch-to-next-input-method)

;;; TRAMP configuration
(eval-after-load "tramp"
  '(progn
	 ;; history file
	 (setq password-cache-expiry 240 ; 5 min
		   password-cache t
		   tramp-syntax 'default)
	 ;; windows configuration - use plink by default
	 (when (system-is-windows)
	   (when (executable-exists-p "plink")
		 (setq tramp-default-method "plink")
		 ;;(custom-set-variables '(tramp-local-end-of-line (eval "\n")))
		 ))))

;; do not create multiple windows when using ediff
(eval-after-load "ediff"
  '(setq ediff-window-setup-function 'ediff-setup-windows-plain))

;; create notes buffer
(add-to-list 'after-init-hook #'make-notes-buffer)
(global-set-key (kbd "C-x n") 'get-notes-buffer)

;; window management using mouse with extra keys
;(setq mouse-autoselect-window t)
(global-set-key (kbd "<mouse-8>") 'split-window-below)
(global-set-key (kbd "<mouse-9>") 'split-window-right)
(global-set-key (kbd "<S-mouse-2>") 'delete-window)
