;;;; TeX mode configuration

(eval-after-load "reftex"
  '(setq reftex-plug-into-AUCTeX t))

;;; try to find master file among emacs buffers
(defun guess-tex-master (filename)
  "Guess the master file for FILENAME from currently open .tex files."
  (let ((candidate nil)
		(filename (file-name-nondirectory filename)))
	(save-excursion
	  (dolist (buffer (buffer-list))
		(with-current-buffer buffer
		  (let ((name (buffer-name))
				(file buffer-file-name))
			(if (and file (string-match "\\.tex$" file))
				(progn
				  (goto-char (point-min))
				  (if (re-search-forward (concat "\\\\input{" filename "}") nil t)
					  (setq candidate file))
				  (if (re-search-forward (concat "\\\\include{" (file-name-sans-extension filename) "}") nil t)
					  (setq candidate file))))))))
	(if candidate
		(message "TeX master file: %s" (file-name-nondirectory candidate)))
	candidate))

;; (defun ac-LaTeX-mode-setup () ; add ac-sources to default ac-sources
;;   (setq-local ac-sources
;; 			  (append '(ac-source-math-unicode
;; 						ac-source-math-latex
;; 						ac-source-latex-commands)
;; 					  ac-sources))
;;   (mapc #'(lambda (e) ;; remove some useless auto-completion sources
;; 			(delete e ac-sources))
;; 		'(ac-source-words-in-same-mode-buffers))
;;   (auto-complete-mode))

(with-eval-after-load 'company-math
  (setq company-math-allow-latex-symbols-in-faces t))

(defun company-LaTeX-mode-setup ()
  (company-mode 1)
  (setq-local company-backends
			  (append '((company-math-symbols-latex
						 company-latex-commands)
						company-backends)))
  (mapc #'(lambda (e) ;; remove some useless auto-completion sources
			(delete e company-backends))
		'(company-dabbrev)))

;; Synctex  configuration
(defun enable-synctex (&optional viewer)
  "Enable SyncTeX and optionally set the default PDF Viewer."
  (add-hook 'TeX-mode-hook 'TeX-source-correlate-mode)
  (when viewer
	(let ((output-pdf (assq 'output-pdf TeX-view-program-selection)))
	  (cond
	   (output-pdf
		(setcdr output-pdf
				(list viewer)))
	   (t
		(add-to-list 'TeX-view-program-selection
					 (list 'output-pdf viewer)))))))

;; Sumatra is the only (?) viewer with SyncTeX support on Windows.
(defun sumatra-installed-p ()
  "Try to check if Sumatra PDF installed."
  (when (system-is-windows)
	(and (win-reg-read-64-or-32 "HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Applications\\SumatraPDF.exe\\Shell\\Open\\Command") t)))

;; Fix for XeTeX. An XDV file is an intermediate before generating a PDF.
(defun xdv2pdf ()
  (let ((out-extension (TeX-output-extension)))
	(if (string= out-extension "xdv")
		(progn
		  (TeX-process-set-variable (TeX-active-master)
									'TeX-output-extension
									"pdf")
		  "pdf")
	  out-extension)))

(with-eval-after-load 'tex
  (require 'auctex-latexmk) ; for TeX-run-latexmk
  ;; general configuration
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  ;; enable PDF mode by default
  (setq TeX-PDF-mode t)
  ;; fix for XeTeX
  (setq TeX-view-predicate-list '((output-pdf
								   (string-match "pdf"
												 (xdv2pdf)))))
  ;; enable reftex
  (add-hook 'TeX-mode-hook 'turn-on-reftex)
  (add-hook 'TeX-mode-hook (lambda ()
							 (visual-line-mode t)))
  (add-hook 'TeX-mode-hook #'(lambda ()
							   (setq-local TeX-master (guess-tex-master (buffer-file-name)))))
  ;; use latexmk to build latex documents
  (when (executable-exists-p "latexmk")
	(push
	 '("latexmk-pdfLaTeX"
	   "latexmk -bibtex -file-line-error -interaction=nonstopmode -synctex=1 -pdf -f %s"
	   TeX-run-latexmk nil t
	   :help "Run latexmk to build PDF file using pdfLaTeX.")
	 TeX-command-list)
	(push
	 '("latexmk-XeLaTeX"
	   "latexmk -bibtex -file-line-error -interaction=nonstopmode -synctex=1 -xelatex -f %s"
	   TeX-run-latexmk nil t
	   :help "Run latexmk to build PDF file using XeLaTeX.")
	 TeX-command-list)
	(push
	 '("latexmk-LuaLaTeX"
	   "latexmk -bibtex -file-line-error -interaction=nonstopmode -synctex=1 -lualatex -f %s"
	   TeX-run-latexmk nil t
	   :help "Run latexmk to build PDF file using LuaLaTeX.")
	 TeX-command-list)
	;; (push
	;;  '("latexmk-clean"
	;;    "latexmk -pdf -c %s"
	;;    TeX-run-latexmk nil t
	;;    :help "Clean results of document compilation using latexmk.")
	;;  TeX-command-list)
	(add-hook 'TeX-mode-hook #'(lambda ()
								 (setq-local TeX-command-default "latexmk-XeLaTeX"))))
  ;; detect common viewers with SyncTeX support
  (cond
   ((and (system-is-windows)
		 (sumatra-installed-p))
	(add-to-list 'TeX-view-program-list '("Sumatra PDF" ("start SumatraPDF -reuse-instance " (mode-io-correlate " -forward-search %b %n -inverse-search \"emacsclientw --no-wait +%%l \\\"%%f\\\"\" ") " %o")))
	(enable-synctex "Sumatra PDF"))
   ((executable-exists-p "evince")
	(enable-synctex "Evince"))
   (t nil)))

(defun latex-run-last-command ()
   (interactive)
   (TeX-command-run-all nil))

(defun latex-compile-keys ()
  (local-set-key (kbd "<f5>") 'latex-run-last-command)
  (local-set-key (kbd "<S-f5>") 'TeX-command-master))

(with-eval-after-load 'latex
  ;; intermediate files
  (setq-default LaTeX-clean-intermediate-suffixes
				(append LaTeX-clean-intermediate-suffixes
						'("\\.xdv" ; XeLaTeX
						  "\\.fdb_latexmk"
						  "\\.aux\\.bak"
						  "\\.fls")))
  ;; enable math mode
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  ;; auto-completion
  ;; enable auto-completion for latex documents
  ;; (require 'ac-math)
  ;; (add-to-list 'ac-modes 'latex-mode)
  ;; (add-hook 'LaTeX-mode-hook 'ac-LaTeX-mode-setup)
  (add-hook 'LaTeX-mode-hook 'company-LaTeX-mode-setup)
  (add-hook 'LaTeX-mode-hook 'latex-compile-keys))
