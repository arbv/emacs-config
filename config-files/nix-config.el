;;; Nix configuration

(add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode))
(add-to-list 'auto-mode-alist '("\\.nix.in\\'" . nix-mode))
(add-to-list 'auto-mode-alist '("\\.drv\\'" . nix-drv-mode))

(cond
 ((executable-find "nixpkgs-fmt")
  (setq nix-nixfmt-bin "nixpkgs-fmt")
  (add-hook 'before-save-hook 'nix-format-before-save))
 ((executable-find "nixfmt")
  (add-hook 'before-save-hook 'nix-format-before-save))
 (t nil))


(with-eval-after-load 'eglot
  (when (executable-find "nil")
    (add-to-list 'eglot-server-programs '(nix-mode . ("nil")))))

(defun arbv/nix-mode-setup ()
  (when (executable-find "nil")
    (eglot-common-setup)))

(with-eval-after-load 'nix-mode
  (add-to-list 'nix-mode-hook 'arbv/nix-mode-setup))
