;;; EShell configuration ;;;
(eval-after-load "eshell"
  '(progn
	 ;; change subprocess codepage on windows
	 (when (system-is-windows)
	   (add-hook 'eshell-mode-hook
				 (lambda ()
				   (setq-local default-process-coding-system (cons +windows-subprocess-codepage+
																   +windows-subprocess-codepage+))
				   )))

	 ;; update PATH on EShell start
	 (add-hook 'eshell-mode-hook
			   (lambda ()
				 (setq eshell-path-env (getenv "PATH"))))

	 ;; change EShell prompt function to the BASH like (except full path)
	 (setq eshell-prompt-function
		   (lambda ()
			 (concat
			  user-login-name
			  "@"
			  system-name
			  ":"
			  (eshell/basename
			   (abbreviate-file-name (eshell/pwd)))
			  (if (= (user-uid) 0)
				  "#"
				"$")
			  " ")))
	 ;; change EShell prompt regexp
	 (setq eshell-prompt-regexp "^[^#$\n]*[#$] ")))

;; clear command for EShell
(defun eshell/clear ()
	"Clear EShell buffer."
	(let ((eshell-buffer-maximum-lines 0))
	  (eshell-truncate-buffer)))

;; handy alias for clear command
(defalias 'eshell/cls 'eshell/clear)
