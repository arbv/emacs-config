;; !!! GUI setup !!!
(eval-when-compile (require 'cl))

;(defconst +color-theme+ 'adwaita)

; disable dialog box
(setq use-dialog-box nil)
; disabel ring
(setq ring-bell-function 'ignore)

;; frame setup
;(defun frame-setup (frame)
  ;; disable toolbar
  ;(when (is-gui-running)
	;(tool-bar-mode -1)))

;(add-hook 'after-make-frame-functions #'frame-setup)
(tool-bar-mode -1)

;;; color theme setup
;; threat all custom themes as safe
(setq custom-safe-themes t)
;; disable colorthemes before setting another one
;; (defadvice load-theme (before theme-dont-propagate activate)
;;   (unless custom-theme-allow-multiple-selections
;; 	(mapc #'disable-theme (custom-available-themes))))

;(load-theme 'sanityinc-tomorrow-day t)
;(load-theme 'sanityinc-solarized-light t)
;(load-theme +color-theme+ t)

;; toa void theme colors mixing
;;(add-hook 'after-init-hook #'reapply-current-theme)

;; !!! Fonts setup !!!
; Preferred fonts list
(defconst +preferred-fonts+
  '(("Consolas" . 12)
	("PT Mono" . 12)
	("DejaVu Sans Mono" . 10)
	("Courier New" . 10)
	("Terminus" . 10)
	("Mono" . 10)))

; convert font name and its size into string for set-default-font
(defun make-font-string (name size)
  (concat name "-" (number-to-string size)))

;;; Set font globally
(defun set-font-globally (font)
  ;(add-to-list 'default-frame-alist (cons 'font font))
  (set-face-attribute 'default t :font font)
  (set-face-attribute 'default nil :font font)
  (set-frame-font font nil t))

;; Try list of preferred fonts
(dolist (f +preferred-fonts+)
  (when (font-exists-p (car f))
	(set-font-globally (make-font-string (car f) (cdr f)))
	(cl-return)))


;;; Disable menu bar in fullscreen mode ;;;
(defvar *hide-menu-bar-in-fullscreen-mode* t)
(defvar *menu-bar-was-enabled* (bound-and-true-p menu-bar-mode))

(defun current-frame-fullscreen-p ()
  "Return T if current frame is fullscreen."
  (let ((fullscreen-param (frame-parameter nil 'fullscreen)))
	(or (eq 'fullscreen fullscreen-param)
		(eq 'fullboth fullscreen-param))))

(defun current-frame-has-menubar-p ()
  "Returns true, if current frame has menu bar."
  (cl-plusp (frame-parameter nil 'menu-bar-lines)))

(defun current-frame-enable-menubar-p (param)
  "Enable menu bar for current frame if PARAM is positive."
  (if (cl-plusp param)
	  (set-frame-parameter nil 'menu-bar-lines 1)
	(set-frame-parameter nil 'menu-bar-lines 0))
  (redraw-frame))

(defun advice-toggle-fullscreen-disable-menu (orig &rest args)
  (when *hide-menu-bar-in-fullscreen-mode*
	;;(make-variable-frame-local '*menu-bar-was-enabled*)
	(set-frame-parameter nil '*menu-bar-was-enabled* (current-frame-has-menubar-p))
	(cond
	 ;; we are going from fullscreen mode
	 ((current-frame-fullscreen-p)
	  (when (or *menu-bar-was-enabled*
				(current-frame-has-menubar-p))
		(current-frame-enable-menubar-p 1)))
	 ;; we are going to fullscreen mode
	 (t
	  (setq *menu-bar-was-enabled* (current-frame-has-menubar-p))
	  (when (current-frame-has-menubar-p)
		(current-frame-enable-menubar-p 0)))))
  (apply orig args))

(advice-add 'toggle-frame-fullscreen :around #'advice-toggle-fullscreen-disable-menu)

(when (arbv/check-configuration-feature 'lucid)
  ;; set scroll-bar mode to right when using LUCID toolkit
  (set-scroll-bar-mode 'right))
