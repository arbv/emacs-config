;; Elisp

(defun elisp-mode-setup ()
  ;; SLIME-like Elisp navigation
  (elisp-slime-nav-mode 1)
  ;; Auto-complete
  (company-mode 1)
  (make-local-variable 'company-backends)
  (cl-pushnew 'company-elisp company-backends))

(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook #'elisp-mode-setup))
