;;; This file contains common routines for Emacs

(eval-when-compile (require 'cl))

;; OS detection
(defun system-is-windows ()
	(string-equal system-type "windows-nt"))

(defun system-is-linux ()
	(string-equal system-type "gnu/linux"))

(defun system-is-osx ()
  (string-equal system-type "darwin"))

;; check if gui is running
(defun is-gui-running ()
  (not (null window-system )))

;; check if directory exists
(defun directory-exists-p (path)
  (and (file-exists-p path) (file-directory-p path)))

(defun directory-empty-p (path)
  "Returns non-NIL value if PATH is an empty directory."
  (when (directory-exists-p path)
	(let ((contents (directory-files path)))
	  (not (not (and
				 (= (length contents) 2)
				 (cl-member-if (lambda (e)
							  (string= e "."))
							contents)
				 (cl-member-if (lambda (e)
							  (string= e ".."))
							contents)))))))

;; check if executable exists
(defun executable-exists-p (name)
  (not (null (executable-find name))))

;; iterate over list
(defun for-each (proc lst)
  (dolist (e lst)
	(funcall proc e)))

;; call with escaping (one way) continuation
; requires 'cl package for closures suport
(defun call-with-escaping-continuation (func)
  (lexical-let* ( (sym (cl-gensym)) ; random symbol
				  (ec  (lambda (res)
						 (throw sym res))))
	(catch sym (funcall func ec))))
										; alias call/ec
(defalias 'call/ec 'call-with-escaping-continuation)

;; check if font exists
(defun font-exists-p (font-name)
  (not (null (find-font (font-spec :name font-name)))))

;; reload Emacs config
(defun reload ()
  (interactive)
  (let ( (emacs-rc    "~/.emacs")
		 (emacs-init  (concat user-emacs-directory "init.el")) )
	(cond
	 ((file-exists-p emacs-rc) (load emacs-rc))
	 ((file-exists-p emacs-init) (load emacs-init)))))

;; clear shellish buffer
(defun clear-comint-buffer ()
  (interactive)
  (let ((old-max comint-buffer-maximum-size))
	(setq comint-buffer-maximum-size 0)
	(comint-truncate-buffer)
	(setq comint-buffer-maximum-size old-max)
	(goto-char (point-max))))

(defun system-path-to-emacs-path (file-name)
  "Convert system path to the path suitable for usage in Emacs."
  (cond
   ((system-is-windows)
	(replace-regexp-in-string "\\\\" "/" file-name))
   (t file-name)))

(defun emacs-path-to-system-path (file-name)
  "Convert Emacs path to the path suitable for usage by host OS."
  (cond
   ((system-is-windows)
	(replace-regexp-in-string "/" "\\\\" file-name))
   (t file-name)))

(defun get-home-directory ()
  "Get home directory"
  (let ((emacs-home-dir (file-name-as-directory (getenv "HOME"))))
	(cond
	 ((system-is-windows)
	  (let ((user-profile-path (getenv "USERPROFILE")))
		(file-name-as-directory (system-path-to-emacs-path user-profile-path))))
	 (t emacs-home-dir))))

;; open config .emacs.d directory in dired
(defun dired-config ()
  "Open emacs config directory in dired"
  (interactive)
  (dired user-emacs-directory))

(defun dired-home ()
  "Open user home directory in dired"
  (interactive)
  (dired (get-home-directory)))

;;; Find one of executables from list
;; Args:
;; lst - assoc list of executable names and arguments
(defun find-one-of-executables (lst)
  (when (consp lst)
	(call/ec (lambda (ec)
			   (dolist (elem lst)
				 (let ((exe-name (car elem))
					   (exe-args (cdr elem)))
				   (when (executable-exists-p exe-name)
					 (funcall ec (cons (executable-find exe-name)
									   exe-args)))))))))

(defun check-executable-file-p (path exe-name)
  "Returns path to the executable if found. NIL otherwise."
  (locate-file exe-name (list path) exec-suffixes 1))

(defun find-rest (fn lst)
  (do ((body (cdr lst) (cdr body))
	   (head  (car lst) (car body)))
	  ((null head))
	(when (funcall fn head)
	  (return body))))

(defun reapply-current-theme ()
  "Apply current theme again - hack to avoid color rendering problems"
  (interactive)
  (if custom-enabled-themes
	  (let ((current-theme (car custom-enabled-themes)))
		(dolist (theme custom-enabled-themes) ; disable all active themes
		  (disable-theme theme))
		(load-theme current-theme t))
	(disable-all-themes)))

(defun open-file-in-external-program (file-path)
  "Opens file in (default) external program."
  (cond
   ((system-is-windows)
	(w32-shell-execute "open" (replace-regexp-in-string "/" "\\" file-path t t))
	t)
   ((system-is-linux)
	(call-process "xdg-open" nil 0 nil file-path)
	t)
   ((system-is-osx)
	(shell-command (format "open \"%s\"" file-path))
	t)
   (t nil)))

(defun major-mode-active-p (mode-name)
  "Check if major mode specified in MODE-NAME string is active for current buffer."
  (string-equal major-mode mode-name))

;; http://emacs.stackexchange.com/questions/13713/how-to-disable-ido-in-dired-create-directory
(defun temporarily-disable-ido-advice (func &rest args)
  "Temporarily disable IDO and call function FUNC with arguments ARGS."
  (interactive)
  (let ((read-file-name-function #'read-file-name-default))
	(if (called-interactively-p 'any)
		(call-interactively func)
	  (apply func args))))

(defun disable-ido-advice (command)
  "Disable IDO when COMMAND is called."
  (advice-add command :around #'temporarily-disable-ido-advice))

(defun terminal-frame-p (&optional frame)
  "Return T if specified FRAME is terminal frame."
  (null (frame-parameter frame 'display)))

(defun string-starts-with (s begins)
  "Return non-nil if string S starts with BEGINS."
  (cond ((>= (length s) (length begins))
		 (string-equal (substring s 0 (length begins)) begins))
		(t nil)))

(defun string-ends-with (s ending)
  "Return non-nil if string S ends with ENDING."
  (cond ((>= (length s) (length ending))
		 (let ((elength (length ending)))
		   (string= (substring s (- 0 elength)) ending)))
		(t nil)))

(defun buffer-contains-substring (string &optional buffer)
  (with-current-buffer (or buffer
						   (current-buffer))
	(save-excursion
	  (save-match-data
		(goto-char (point-min))
		(search-forward string nil t)))))

(defun http-url-p (url)
  "Return T if URL string is HTTP(S) link."
  (let ((url-lower (downcase url)))
	(or (string-starts-with url-lower "http://")
		(string-starts-with url-lower "https://"))))


(defun %load-theme-advice (func &rest args)
  (unless (bound-and-true-p custom-theme-allow-multiple-selections)
	(disable-all-themes))
  (apply func args))

(advice-add 'load-theme :around #'%load-theme-advice)
(defun disable-all-themes ()
  "Disable all enabled themes."
  (interactive)
  (when (custom-available-themes)
	(unwind-protect
		(progn
		  (advice-remove 'load-theme #'%load-theme-advice)
		  (load-theme (car (custom-available-themes)) t))
	  (advice-add 'load-theme :around #'%load-theme-advice))
	(disable-theme  (car (custom-available-themes))))
  (mapc #'disable-theme custom-enabled-themes))

;; (defvar non-graphic-display-marker nil)
;; (make-variable-buffer-local 'non-graphic-display-marker)

;; (defun display-graphic-p-advice (func &rest args)
;;   (unless non-graphic-display-marker
;;	(apply func args)))

;; (advice-add 'display-graphic-p :around #'display-graphic-p-advice)

;; (defun non-graphic-buffer (&optional non-graphic)
;;   "When called with non-NIL parameter - current buffer will be marked as non graphic one."
;;   (setq-local non-graphic-display-marker (when non-graphic
;;										   t)))

(defun emacs-pid-p (pid &optional check-all-system-processes)
  "Checks if given PID belongs to the Emacs. Returns T on success, NIL otherwise.

If CHECK-ALL-SYSTEM-PROCESSES is not NIL checks list pf all system processes."
  (when (numberp pid)
	(or (eql (emacs-pid) pid)
		(and check-all-system-processes
			 (let ((res (mapcar #'(lambda (current-process-pid)
									(let ((comm (assoc 'comm (process-attributes current-process-pid))))
									  (when (and comm (string-match (downcase "emacs")
																	(downcase (cdr comm))))
										current-process-pid)))
								(list-system-processes))))
			   (when (member pid res)
				 t))))))

(defun read-lines (filename)
  "Returns a list of lines of a file at filename."
  (with-temp-buffer
	(insert-file filename)
	(goto-char (point-min))
	(let ((lines))
	  (while (not (eobp))
		(let* ((lbegin (point))
			   (lend (progn
					   (forward-line 1)
					   (1- (point)))))
		  (if (= lbegin lend)
			  (push "" lines)
			(push (buffer-substring lbegin lend) lines))))
	  (nreverse lines))))

(defun server-file-pid (&optional file-path)
  "Read PID from Emacs server file."
  (let ((fname (if file-path
				   file-path
				 (expand-file-name server-name server-auth-dir))))
	(when (file-exists-p fname)
	  (let ((contents (read-lines fname)))
		(when (and contents (plusp (length contents)))
		  (let ((connection-data (split-string (car contents) " " t)))
			(when (>= (length connection-data) 2)
			  (string-to-number (cadr connection-data)
								10))))))))

(defun buffer-mode (&optional buffer-or-string)
  "Returns the major mode associated with a buffer."
  (with-current-buffer (if buffer-or-string
						   buffer-or-string
						 (window-buffer (minibuffer-selected-window)))
	major-mode))

(cl-defun program-running-p (name)
  "Returns T if process with executable named NAME exists."
  (check-type name string)
  (let ((pid-list (list-system-processes)))
	(dolist (pid pid-list)
	  (let ((comm (assoc 'comm (process-attributes pid))))
		(when comm
		  (let ((pid-exe-name (cdr comm)))
			(if (system-is-windows)
				(let ((pid-exe-name (downcase pid-exe-name))
					  (name (downcase name)))
				  (when (or (string= name pid-exe-name)
							(string= (concat name ".exe")
									 pid-exe-name)
							(string= (concat name ".com")
									 pid-exe-name))
				  (return-from program-running-p t)))
			  (when (string= name pid-exe-name)
				(return-from program-running-p t)))))))))

;; This is utilities to toggle displaying of the images in a current buffer.
;; It is mostly for usage in EWW for HyperSpec lookup.

(defun backup-display-property (invert &optional object)
  "Move the 'display property at POS to 'display-backup.
Only applies if display property is an image.
If INVERT is non-nil, move from 'display-backup to 'display
instead.
Optional OBJECT specifies the string or buffer. Nil means current
buffer."
  (let* ((inhibit-read-only t)
		 (from (if invert 'display-backup 'display))
		 (to (if invert 'display 'display-backup))
		 (pos (point-min))
		 left prop)
	(while (and pos (/= pos (point-max)))
	  (if (get-text-property pos from object)
		  (setq left pos)
		(setq left (next-single-property-change pos from object)))
	  (if (or (null left) (= left (point-max)))
		  (setq pos nil)
		(setq prop (get-text-property left from object))
		(setq pos (or (next-single-property-change left from object)
					  (point-max)))
		(when (eq (car prop) 'image)
		  (add-text-properties left pos (list from nil to prop) object))))))

(defvar-local display-images-switch t)

(defun toggle-image-display ()
  "Toggle images display on current buffer."
  (interactive)
  (setq display-images-switch
		(null display-images-switch))
  (backup-display-property display-images-switch))

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
	(insert-file-contents filePath)
	(buffer-string)))

(defun write-string-to-file (file-path str)
  "Write a string STR to a file. Recreates the file if it exists."
  (when (and (stringp file-path)
			 (stringp str))
	(with-temp-buffer
	  (insert str)
	  (when (file-exists-p file-path)
		(delete-file file-path))
	  (append-to-file nil nil file-path)
	  t)))

(defun play-ding ()
  (let ((ring-bell-function nil)
		(visible-bell nil))
	(ding)))

(defun create-scratch-buffer ()
  (save-excursion
	(let ((buf (get-buffer-create "*scratch*")))
	  (set-buffer buf)
	  (lisp-interaction-mode)
	  (insert initial-scratch-message)
	  buf)))

(defun rescratch ()
  "Recreate scratch buffer if it is needed to."
  (interactive)
  (let ((scratch (get-buffer "*scratch*")))
	(if scratch
		(switch-to-buffer scratch)
	  (switch-to-buffer (create-scratch-buffer)))))

(defun directory-paths-equal-p (e1 e2)
  (let ((e1 (expand-file-name (directory-file-name (system-path-to-emacs-path e1))))
		(e2 (expand-file-name (directory-file-name (system-path-to-emacs-path e2)))))
	(if (system-is-windows)
		(string-equal e1 e2)
	  (string= e1 e2))))

(defun executable-path-p (path)
  "Returns T if specified path is an executable path."
  (let* ((path (directory-file-name (system-path-to-emacs-path path))))
	(cl-member-if #'(lambda (e)
					  (directory-paths-equal-p path e))
				  exec-path)))

(defun path-variable-name ()
  (if (system-is-windows)
	  "Path"
	"PATH"))

(defun add-executable-path (path &optional append)
  "Add executable path to both EXEC-PATH and PATH environmental variable."
  (let* ((path (directory-file-name (system-path-to-emacs-path path))))
	(unless (executable-path-p path)
	  ;; Add to Emacs executable paths
	  (add-to-list 'exec-path path append)
	  ;; Set system wide variables
	  (let ((pathvar-name (path-variable-name)))
		(if append
			(if (string-suffix-p path-separator (getenv pathvar-name))

				(setenv pathvar-name (concat (getenv pathvar-name)
											 (emacs-path-to-system-path path)
											 path-separator))
			  (setenv pathvar-name (concat (getenv pathvar-name)
										   path-separator
										   (emacs-path-to-system-path path)
										   path-separator)))
		  (setenv pathvar-name (concat (emacs-path-to-system-path path)
									   path-separator
									   (getenv pathvar-name)))))
	  ;; Just in case. Access to PATH variable seems to be virtualised in Emacs (It always equals "Path").
	  (when (system-is-windows)
		(setenv "PATH" (getenv "Path"))))))

(defun remove-executable-path (path)
  "Removes executable path from both EXEC-PATH and PATH environmental variable."
  (let* ((path (directory-file-name (system-path-to-emacs-path path))))
	(when (executable-path-p path)
	  (cl-delete path exec-path :test #'directory-paths-equal-p)
	  (setenv (path-variable-name)
			  (cl-reduce #'(lambda (e1 e2)
							 (concat e1 path-separator e2))
						 (cl-remove path
									(split-string (getenv (path-variable-name))
												  path-separator)
									:test #'directory-paths-equal-p)))
	  (when (system-is-windows)
		(setenv "PATH" (getenv "Path"))))))

(defun get-unix-time-sec ()
  "Returns number of seconds from 1970-01-01 00:00:00 +0000."
  (car (read-from-string (format-time-string "%s" (current-time)))))

(defun make-empty-buffer (name &optional major-mode-func offer-save)
  "Creates new empty buffer."
  (let* ((name (or name "untitled"))
		 (buf (generate-new-buffer name)))
	(with-current-buffer buf
	  (when major-mode-func
		(funcall major-mode-func))
	  (setq-local buffer-offer-save offer-save))
	buf))

(defun make-notes-buffer ()
  (make-empty-buffer "*notes*" #'text-mode))

(defun get-notes-buffer ()
  (interactive)
  (let* ((notes-buffer (get-buffer "*notes*")))
	(if notes-buffer
		(switch-to-buffer notes-buffer)
	  (switch-to-buffer (make-notes-buffer)))))

(defun win-reg-read (regkey &optional value access-64-bit)
  "Read a value in the Windows registry. If the value does not exist, it returns nil. For an empty REG_BINARY value it returns T."
  (when (string-equal system-type "windows-nt")
	(let* ((regkey (encode-coding-string regkey locale-coding-system))
		   (value (and value (encode-coding-string value locale-coding-system)))
		   (reg-exe (concat (getenv "WinDir") "\\System32\\reg.exe"))
		   (bits-64-param (if access-64-bit
							  "/reg:64"
							"/reg:32"))
		   (reg-value (let ((output (with-output-to-string
									  (with-current-buffer
										  standard-output
										(if value
											(process-file reg-exe nil t nil
														  "query"
														  regkey
														  "/v"
														  value
														  bits-64-param)
										  (process-file reg-exe nil t nil
														"query"
														regkey
														"/ve"
														bits-64-param))))))
						(set-text-properties 0 (length output) nil output)
						output)))
	  (let* ((lines (split-string reg-value "\n" nil nil))
			 (result-line (nth 2 lines)))
		(when (> (length lines) 4)
		  (let* ((result-line (if value ; skip value name
								  (cl-subseq result-line (+ 8 (length value)))
								(replace-regexp-in-string "^[ ]+[\(][^\)]+[\)][ ]+" ; default value
														  ""
														  result-line)))
				 (tokens (split-string result-line "    " t))
				 (type-string (car tokens)) ; get registry value type
				 (result (cl-subseq result-line (+ 4 (length type-string))))) ; get registry value
			;; decode the registry value string into a lisp object
			(cond
			 ((or (string-equal type-string "REG_DWORD")
				  (string-equal type-string "REG_QWORD"))
			  (string-to-number (cl-subseq result 2) 16))
			 ((or (string-equal type-string "REG_SZ")
				  (string-equal type-string "REG_EXPAND_SZ"))
			  result)
			 ((string-equal type-string "REG_MULTI_SZ")
			  (split-string result (regexp-quote "\\0") t)) ; will not work if there are '\0' sequences in the string
			 ((string-equal type-string "REG_BINARY")
			  (let ((res)
					(pos 0)
					(size (/ (length result) 2)))
				(if (zerop size)
					t ; value is present, but empty
				  (progn
					(dotimes (i size)
					  (push (string-to-number (cl-subseq result pos (+ pos 2)) 16) res)
					  (incf pos 2))
					(reverse res)))))
			 (t nil))))))))

(defun win-reg-read-64-or-32 (regkey &optional value)
  "Read a value in the Windows registry. If the value does not exist, it returns nil. Checks both 32-bit and 64-nit hives, considers system bitness when searching for a value."
  (let ((check (getenv "ProgramW6432")))
	(if (or (null check) (zerop (length check)))
		(win-reg-read regkey value nil) ; 32-bit Windows
	  (or (win-reg-read regkey value t) ; 64-bit Windows
		  (win-reg-read regkey value nil)))))

(defun ncpu ()
  "Returns number of CPUs (cores) on the system."
  (string-to-number
   (cond
	((system-is-linux)
	 (shell-command-to-string "cat /proc/cpuinfo | grep -P \"processor\t:\" | wc -l"))
	((system-is-osx)
	 (shell-command-to-string "sysctl -n hw.logicalcpu"))
	((system-is-windows)
	 (getenv "NUMBER_OF_PROCESSORS"))
	(t 1))))

(defun arbv/loop-over-directory (dirname fn)
  "Execute the function in every file name in the directory, until it returns false"
  (when (and (file-exists-p dirname)
             (file-directory-p dirname))
    (lexical-let ((files (directory-files dirname)))
      (call/ec #'(lambda (stop)
                   (dolist (file-name files)
					 (let ((full-path (expand-file-name (concat (file-name-as-directory dirname)
																file-name))))
                       (unless (funcall fn full-path)
                         (funcall stop t)))))))))

(defun arbv/check-configuration-feature (feature)
  (let ((features  (car (read-from-string (concat "(" system-configuration-features ")")))))
    (seq-reduce #'(lambda (a1 a2)
					(or a1 a2))
				(mapcar #'(lambda (e)
							(eq feature e))
						features)
				nil)
    ))
