(eval-and-compile
  (require 'cedet)
;;  (require 'srecode)
  (require 'semantic)
  (require 'semantic/bovine/gcc)
  (require 'semantic/ia)
  )
;;; CEDET configuration

(add-to-list 'semantic-default-submodes 'global-semantic-mru-bookmark-mode)
(add-to-list 'semantic-default-submodes 'global-semanticdb-minor-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-scheduler-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-stickyfunc-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-highlight-func-mode)

;; SRecode
;;(global-srecode-minor-mode 1)

;; turn on semantic
(semantic-mode 1)

(defun c-semantic-setup ()
  (semantic-gcc-setup)
  (add-to-list 'ac-sources 'ac-source-functions)
  (add-to-list 'ac-sources 'ac-source-semantic))

;; enable semantic mode
(add-hook 'c-mode-common-hook 'c-semantic-setup)
