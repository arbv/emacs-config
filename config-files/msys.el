;;;; Configuration for using MSYS on Windows

(defvar *msys-bin-path* nil)

(defun find-and-add-msys-path ()
  (let ((msys-path-env (getenv "MSYS_PATH")))
    (if (and msys-path-env
             (directory-exists-p (file-name-as-directory
                                  (concat (file-name-as-directory (file-name-directory (system-path-to-emacs-path msys-path-env)))
                                          "bin"))))
        (progn
          (add-executable-path msys-path-env)
          (setf *msys-bin-path* (file-name-as-directory (concat (file-name-as-directory (file-name-directory (system-path-to-emacs-path msys-path-env)))
                                                                "bin")))
          (add-executable-path  *msys-bin-path*))
      (progn
        (let ((msys-exe-path (executable-find "msys.bat")))
          (when msys-exe-path
            (setf *msys-bin-path* (file-name-as-directory (concat (file-name-directory (system-path-to-emacs-path msys-exe-path))
                                                                  "bin"))) 
            (add-executable-path *msys-bin-path*)))))))

(defun are-unix-tools-in-path-p ()
  (and (executable-exists-p "ls")
       (executable-exists-p "cat")
       (executable-exists-p "sh")
       (executable-exists-p "rm")
       (executable-exists-p "mv")))

;; find MSYS
(unless (are-unix-tools-in-path-p)
  (find-and-add-msys-path))

;; set some environmental variables
(when (are-unix-tools-in-path-p)
  (unless (getenv "OSTYPE")
    (setenv "OSTYPE" "msys"))
  (unless (getenv "MAKE_MODE")
    (setenv "MAKE_MODE" "unix"))
  (unless (getenv "MSYSTEM")
    (setenv "MSYSTEM" "MINGW32"))
  (unless (getenv "TERM")
    (setenv "TERM" "cygwin"))
  ;; set GCC as compiler
  (when (executable-exists-p "gcc")
    (setenv "CC" "gcc"))
  (when (executable-exists-p "g++")
    (setenv "CXX" "g++")))

;; set full paths to the 'find' and 'grep' commands
(when (and *msys-bin-path*
           (are-unix-tools-in-path-p))
  (let ((new-grep-program (concat *msys-bin-path* "grep.exe"))
        (new-find-program (concat *msys-bin-path* "find.exe")))
    (when (executable-find new-grep-program)
      (setf grep-program (format "\"%s\""  new-grep-program)))
    (when (executable-find new-find-program)
      (setf find-program (format "\"%s\"" new-find-program)))
    ;; update grep and find settings
    (grep-compute-defaults)))

