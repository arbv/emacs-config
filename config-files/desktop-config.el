;;;; Emacs Desktop -  session management
(eval-and-compile (require 'desktop))

;;; desktop default directory
(defconst +desktop-default-dirname+ (file-name-as-directory (concat user-emacs-directory "desktop")))

(defun guess-desktop-dirname (&optional dirname)
  (or dirname
	  desktop-dirname
	  +desktop-default-dirname+))

;; remove lock file
(defun desktop-unlock (&optional dirname)
  (interactive)
  (let ((lock-path (concat (file-name-as-directory (guess-desktop-dirname dirname))
						   desktop-base-lock-name)))
	(when (file-exists-p lock-path)
	  (delete-file lock-path))))

;; force read session
(defun desktop-force-read (&optional dirname)
  (interactive)
  (desktop-unlock (guess-desktop-dirname dirname))
  (desktop-read (guess-desktop-dirname dirname)))

;; check if current emacs instance is an owner of the desktop file
(defun desktop-owner-p (&optional dirname)
  (emacs-pid-p (desktop-owner (guess-desktop-dirname dirname))
			   t))

(defun desktop-exists-p (&optional dirname)
  (let ((desktop-file-path (concat (guess-desktop-dirname dirname)
								   desktop-base-file-name)))
	(file-exists-p desktop-file-path)))

(defun desktop-save-mode-enabled-p ()
  (bound-and-true-p desktop-save-mode))

(defun handle-desktop-save ()
  ;; create desktop file if it does not exist (e.g. on a new installation)
  (unless (desktop-exists-p)
	(desktop-save-in-desktop-dir)
	(desktop-unlock)))

(defun handle-desktop-after-read ()
  ;; reapply saved themes
  (dolist (theme custom-enabled-themes)
	(load-theme theme))
  (desktop-save-mode t))

(defun load-desktop-on-startup ()
  ;; unlock desktop file when Emacs was closed in irregular way.
  (let ((found-desktop-dir (locate-dominating-file default-directory
												   desktop-base-file-name)))
	(when found-desktop-dir
	  (setq
	   desktop-dirname found-desktop-dir
	   desktop-path    (list found-desktop-dir)))
	(unless (desktop-owner-p found-desktop-dir)
	  (desktop-unlock found-desktop-dir))
	(unless (desktop-read found-desktop-dir)
	  (desktop-save-mode-off))))

(with-eval-after-load 'desktop
  ;; create desktop directory if it is not exists yet
  (unless (directory-exists-p +desktop-default-dirname+)
	(make-directory +desktop-default-dirname+))
  (setq
   desktop-dirname               +desktop-default-dirname+
   desktop-path                  (list +desktop-default-dirname+)
   desktop-base-file-name        "emacs.desktop"
   desktop-base-lock-name        "emacs.desktop-lock"
   desktop-save                  t
   desktop-files-not-to-save (concat
							  "\\("
							  desktop-files-not-to-save
							  "\\)"
							  "\\|"
							  "\\("
							  "^$"
							  "\\)")
   desktop-buffers-not-to-save (concat
								"\\("
								desktop-buffers-not-to-save ; default regex
								"\\)"
								"\\|"
								"\\("
								"\\/\\(\\(|?[a-zA-Z]+\\:\\(.+\\@\\)?.+\\:\\)+\\)+\\/.*" ; do not save TRAMP buffers (named like "/ssh:user@server:|sudo:server:/home/user/file")
								"\\)")
   desktop-clear-preserve-buffers (remove "\\*tramp/.+\\*" desktop-clear-preserve-buffers)
   desktop-load-locked-desktop   nil)
  (add-to-list 'desktop-modes-not-to-save 'eww-mode)
  ;; save currently enabled themes
  (add-to-list 'desktop-globals-to-save 'custom-enabled-themes)
  ;; re-apply emacs theme after desktop has been loaded (hack to avoid rendering problems)
  ;; (add-hook 'desktop-after-read-hook #'(lambda ()
  ;;											(when (desktop-owner-p)
  ;;											  (reapply-current-theme)))) ; apply current theme again
  (add-hook 'desktop-after-read-hook #'handle-desktop-after-read))
;; autosave desktop on exit
(add-hook 'emacs-startup-hook #'load-desktop-on-startup)
(add-hook 'kill-emacs-hook #'handle-desktop-save)
