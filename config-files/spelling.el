;;;; Emacs spell checking configuration

(defconst +ispell-program-and-args+ (list "hunspell"))
(defvar *flyspell-prog-mode* nil)

(defun ispell-program-available-p ()
  (let ((program (car +ispell-program-and-args+)))
	(and program
		 (or (executable-find program)
			 (file-executable-p program)))))

(defun update-flyspell-modeline ()
  (when (or ispell-local-dictionary
			ispell-dictionary)
	(setq flyspell-mode-line-string (format " Fly(%s)" (or ispell-local-dictionary
														   ispell-dictionary)))
	(force-mode-line-update)))

;; switch spell checking dictionary
(defun ispell-switch-dictionary ()
  (interactive)
  (when (ispell-program-available-p)
	(let ((dicts (find-rest #'(lambda (ispell-dic)
								(string= (car ispell-dic) ispell-current-dictionary))
							ispell-local-dictionary-alist)))
	  (if (null dicts)
		  (ispell-change-dictionary (caar ispell-local-dictionary-alist))
		(ispell-change-dictionary (caar dicts)))
	  (let ((flyspell-issue-message-flag t)) ;; issue message when manually switching dictionaries.
		(unless *flyspell-prog-mode*
		  (flyspell-buffer)))
	  (update-flyspell-modeline))))

(eval-after-load "ispell"
;;; configure spell checking
  '(unless (null +ispell-program-and-args+)
	 (let ((program (car +ispell-program-and-args+))
		   (args    (cdr +ispell-program-and-args+)))

	   ;; set executable and arguments
	   (setq ispell-program-name program)
	   (setq ispell-extra-args   args)

	   ;; hunspell settings
	   (setq ispell-really-aspell nil
			 ispell-really-hunspell t)

	   ;; set dictionaries
	   (setq ispell-local-dictionary-alist
			 '(("en_GB"
				"[A-Za-z]" "[^A-Za-z]"
				"[']" nil ("-d" "en_GB") nil iso-8859-1)

			   ("ru_RU"
				"[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
				"[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
				"[-]" nil ("-d" "ru_RU,en_GB") nil utf-8)

			   ("uk_UA"
				"[АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯАабвгґдеєжзиіїйклмнопрстуфхцчшщьюя]"
				"[^АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯАабвгґдеєжзиіїйклмнопрстуфхцчшщьюя]"
				"[-']" nil ("-d" "uk_UA,en_GB") nil utf-8)
			   ))

	   ;; Set first ispell dictionary as default one
	   (unless (null ispell-local-dictionary-alist)
		 (setq ispell-local-dictionary (caar ispell-local-dictionary-alist)))

	   ;; fix if flyspell superseeds autocomplete
	   (when (fboundp 'ac-flyspell-workaround)
		 (ac-flyspell-workaround)))))

;; disable Ctrl+Alt+i
(eval-after-load 'flyspell '(define-key flyspell-mode-map (kbd "C-M-i") nil))

(eval-after-load "flyspell"
  '(progn

	 ;; disable useless FlySpell messages.
	 (setq flyspell-issue-message-flag nil)
	 ;; always treat regions as large - performance optimisation
	 (setq flyspell-large-region 1)
	 ;; fix flyspell for older versions of Emacs.
	 (when (or (< emacs-major-version 26)
			   (and (= emacs-major-version 26)
					(< emacs-minor-version 2)))
	   (load-user-file "config-files/flyspell-fix"))
	 (add-hook 'flyspell-mode-hook 'update-flyspell-modeline)
	 (add-hook 'flyspell-prog-mode-hook 'update-flyspell-modeline)))

;; Use F2
(global-set-key [f2] 'ispell-switch-dictionary)
(when +ispell-program-and-args+
  ;; Flyspell activation for text mode
  (add-hook 'text-mode-hook '(lambda ()
							   (unless (or (> (buffer-size) large-file-warning-threshold)
										   (major-mode-active-p 'fundamental-mode)
										   (not (ispell-program-available-p)))
								 ;; prevents Emacs from hanging while using Magit
								 (if (or (bound-and-true-p git-commit-mode)
										 (string= (buffer-name) "COMMIT_EDITMSG"))
									 (when (fboundp 'git-commit-turn-on-flyspell)
									   (git-commit-turn-on-flyspell))
								   (flyspell-mode t)
								   (flyspell-buffer)))))
  ;; Spelling checking
  (add-hook 'prog-mode-hook '(lambda ()
							   (unless (or (derived-mode-p 'compilation-mode)
										   (not (ispell-program-available-p)))
								 (setq-local *flyspell-prog-mode* t)
								 (flyspell-prog-mode)
								 ;; (let ((inhibit-message t))
								 ;;   (flyspell-buffer))
								 ))))
