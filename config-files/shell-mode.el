(defun enable-shellcheck-if-available ()
  (when (executable-find "shellcheck")
    (flymake-shellcheck-load)
    (flymake-mode t)))

(eval-after-load "sh-mode"
  (add-hook 'sh-mode-hook 'enable-shellcheck-if-available))
