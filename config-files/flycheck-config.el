(defun flycheck-local-keybindings ()
  ;; go to next/previous flycheck error error
  (local-set-key (kbd "M-n") #'flycheck-next-error)
  (local-set-key (kbd "M-p") #'flycheck-previous-error))

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-local-keybindings))
