(defun arbv/haskell-mode-setup ()
  (local-set-key (kbd "C-c C-c") 'haskell-compile)
  (when (executable-find "haskell-language-server-wrapper")
    (eglot-common-setup)))

(with-eval-after-load 'haskell-mode
  (add-hook 'haskell-mode-hook 'arbv/haskell-mode-setup))
