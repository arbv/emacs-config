;;; Emacs Package Configuration

;; ELPA package manager
(eval-and-compile (require 'package))

(defun add-package-repository (name addr)
  (add-to-list 'package-archives (cons name addr)))

(defun all-packages-installed-p (plist)
  "Check if all packages in list are installed"
  (let ((res (mapcar #'(lambda (p)
						 (package-installed-p p)) plist)))
	(cl-reduce #'(lambda (x y) (and x y)) res :initial-value t)))

;; install packages
(defun install-packages (packages)
  "Install list of packages"
  (unless (all-packages-installed-p packages)
	(package-refresh-contents))
  (mapc (lambda (e)
			(unless (package-installed-p e)
			  (package-install e))) packages))

;; Taken from https://emacs.stackexchange.com/questions/16398/noninteractively-upgrade-all-packages
;; with minor modifications.
(defun upgrade-all-packages ()
  "Upgrade all packages automatically without showing *Packages* buffer."
  (interactive)
  (package-refresh-contents)
  (let (upgrades)
	(cl-flet ((get-version (name where)
						   (let ((pkg (cadr (assq name where))))
							 (when pkg
							   (package-desc-version pkg)))))
	  (dolist (package (mapcar #'car package-alist))
		(let ((in-archive (get-version package package-archive-contents)))
		  (when (and in-archive
					 (version-list-< (get-version package package-alist)
									 in-archive))
			(push (cadr (assq package package-archive-contents))
				  upgrades)))))
	(if upgrades
		(when (yes-or-no-p
			   (message "Upgrade %d package%s (%s)? "
						(length upgrades)
						(if (= (length upgrades) 1) "" "s")
						(mapconcat #'package-desc-full-name upgrades ", ")))
		  (save-window-excursion
			(dolist (package-desc upgrades)
			  (let ((old-package (cadr (assq (package-desc-name package-desc)
											 package-alist))))
				(package-install package-desc)
				(package-delete  old-package)))))
	  (message "All packages are up to date."))))

;; automatically update packages
(defconst +package-timestamp-filename+ "last-package-update-check-time")
(defconst +seconds-in-day+ 86400)
(defvar *packages-update-interval-days* 7)

(defun write-current-time-to-timestamp-file ()
  (write-string-to-file
   (concat user-emacs-directory +package-timestamp-filename+)
   (format "%d" (get-unix-time-sec))))

(defun try-to-upgrade-all-packages ()
  (let ((timestamp-file (concat user-emacs-directory +package-timestamp-filename+)))
	(cl-flet ((read-time-from-file ()
								   (when (file-exists-p timestamp-file)
									 (car (read-from-string (get-string-from-file timestamp-file))))))
	  (let* ((last-update-time (read-time-from-file)))
		(when (or (null last-update-time)
				  (< (+ last-update-time
						(* *packages-update-interval-days*
						   +seconds-in-day+))
					 (get-unix-time-sec)))
		  (write-current-time-to-timestamp-file)
		  (upgrade-all-packages))))))

;; Add ELPA repositiries
;;(add-package-repository "marmalade" "https://marmalade-repo.org/packages/")
(setq package-archives nil)
(add-package-repository "melpa" "https://melpa.org/packages/")
(add-package-repository "nongnu" "https://elpa.nongnu.org/nongnu/")
(add-package-repository "gnu" "https://elpa.gnu.org/packages/")
;;(add-package-repository "org" "https://orgmode.org/elpa/")

;; packages to install on startup
(defvar *packages* (cl-remove-if #'null `(;; general
					 ethan-wspace
					 editorconfig
					 company
					 company-quickhelp
					 flycheck
					 elisp-slime-nav
					 project
					 eglot
					 neotree
					 imenu-list
					 deft  ; for search in org-mode files
					 org
					 org-contrib
					 htmlize ; to support exporting code blocks to html in org-mode
					 ,(when (> emacs-major-version 24) 'magit) ; Git porcelain for Emacs
					 ;; TeX support
					 auctex
					 auctex-latexmk
					 company-math
					 ;; Common Lisp development environment
					 slime
					 slime-company
					 ;; Lua support
					 lua-mode
					 flymake-lua
					 company-lua
					 ;; Ruby
					 robe
					 ;; Other
					 markdown-mode
					 php-mode
					 ;; Rust programming language support
					 rust-mode
					 cargo
					 ;; C/C++
					 xcscope
					 ggtags
					 irony
					 company-irony
					 irony-eldoc
					 flycheck-irony
					 clang-format
					 ;; nix
					 nix-mode
					 ;; Shell development
					 flymake-shellcheck
					 ;; F#
					 fsharp-mode
					 eglot-fsharp
					 ;; Haskell
					 haskell-mode
					 ;; Colour themes
					 color-theme-sanityinc-tomorrow
					 color-theme-sanityinc-solarized
					 zenburn-theme
					 cyberpunk-theme
					 minimal-theme
					 gotham-theme
					 ,(when (> emacs-major-version 24) 'zerodark-theme)
					 basic-theme
					 plan9-theme
					 )))

(setq package-enable-at-startup nil)

;; do not ask to update packages on the firs run
(unless (file-directory-p package-user-dir)
  (write-current-time-to-timestamp-file))

(package-initialize)

;; install packages
(install-packages *packages*)

;; Do not ask user to update packages in daemon mode.
(unless (daemonp)
  (try-to-upgrade-all-packages)
  (package-initialize))
