;;;; configuration file for SLIME

(eval-when-compile (require 'cl))
;;; !!! SLIME utilities !!! ;;;

;;; !! execute additional code during system initialization !! ;;;

;; temporary file for user defined constants
(defvar *slime-init-constants* (make-temp-file "slime-constants-" nil ".lisp"))
;; list of common Lisp systems
(defvar *lisps* nil)

(add-hook 'kill-emacs-hook (lambda () ; remove temporary constants file on exit
							 (when (and (bound-and-true-p *slime-init-constants*)
										(file-exists-p *slime-init-constants*))
							   (delete-file *slime-init-constants*))))


(defun slime-init-command-advice (func &rest args)
  (let ((initial-command (apply func args))
		(load-file-path	  (emacs-path-to-system-path
						   (expand-file-name
							(concat
							 user-emacs-directory
							 (file-name-as-directory
							  "misc")
							 "slime-preload.lisp")))))
	(cond
	 ((file-exists-p load-file-path)
	  ;; write constants into temporary file
	  (with-temp-buffer
		(insert
		 (format
		  (concat
		   "(defconstant +slime-user-full-name+ %S)"
		   "(defconstant +slime-user-mail-address+ %S)")
		  user-full-name
		  user-mail-address))
		(write-file *slime-init-constants*))

	  (format (concat "(progn"
					  "(ignore-errors"
					  "(load #P%S)"
					  "(load #P%S))%s)\n\n")
			  (emacs-path-to-system-path *slime-init-constants*)
			  load-file-path
			  (string-trim initial-command)))
	 (t initial-command))))

;; lisp system definition
(defstruct lisp-system
  (name)            ; name of the lisp system
  (exe-names)       ; executable names
  (args)            ; executable arguments
  (coding-system)   ; coding system
  (env))            ; environment

(defmacro deflisp (name exe-names &optional args coding-system env)
  "Add new Lisp system definition."
  (let ((new-lisp (cl-gensym)))
	`(let ((,new-lisp (make-lisp-system :name (quote ,name)
										:exe-names ,exe-names
										:args ,args
										:coding-system (if ,coding-system
														   ,coding-system
														 (quote utf-8-unix))
										:env ,env)))
	   (add-to-list (quote *lisps*)
					,new-lisp
					t
					#'(lambda (e1 e2)
						(when (and e1 e2)
						  (eq (lisp-system-name e1)
							  (lisp-system-name e2))))))))

(defun add-slime-implementation (name program &optional program-args coding-system init init-function env)
  "Add new Lisp implementation to the list of SLIME lisp implementations."
  (let ((impl (list name (append (list program) program-args))))
	(when coding-system
	  (setq impl (append impl (list :coding-system coding-system))))
	(when init
	  (setq impl (append impl (list :init init))))
	(when init-function
	  (setq impl (append impl (list :init-function init-function))))
	(when env
	  (setq impl (append impl (list :env env))))
	(add-to-list 'slime-lisp-implementations
				 impl
				 t
				 #'(lambda (e1 e2)
					 (when (and e1 e2)
					   (eq (car e1)
						   (car e2)))))))

(defun find-lisp-executable (lst)
  (dolist (e lst)
	(when (executable-exists-p e)
	  (return e))))

(defun find-lisps ()
  "Find Lisp systems defined in *LISPS*"
  (dolist (lisp *lisps*)
	(let ((exe (find-lisp-executable (lisp-system-exe-names lisp))))
	  (when exe
		(let ((name (lisp-system-name lisp)))
		  (add-slime-implementation name
									exe
									(lisp-system-args lisp)
									(lisp-system-coding-system lisp)
									nil
									nil
									(lisp-system-env lisp))
		  (unless slime-default-lisp
			(setq slime-default-lisp name)))))))

;;; !!! Configure SLIME !!! ;;;
;; load and configure slime
;; load slime extensions
(defconst +slime-contribs+ '(slime-company
							 slime-repl
							 slime-autodoc
							 slime-references
							 slime-asdf
							 slime-fuzzy
							 slime-fancy-inspector
							 slime-xref-browser
							 slime-trace-dialog
;;							 slime-presentations
							 ))

;; add well known lisp systems
(deflisp ccl64 '("lx86cl64" "wx86cl64" "dx86cl64" "fx86cl64" "ppccl64" "ccl64"))
(deflisp ccl   '("lx86cl" "wx86cl" "dx86cl" "fx86cl" "armcl"  "ppccl" "ccl"))
(deflisp sbcl  '("sbcl") nil nil (when (system-is-windows) ; set HOME variable on Windows
								   (list (concat "HOME="
												 (file-name-as-directory
												  (expand-file-name (get-home-directory)))))))
(deflisp cmucl '("cmucl"))
(deflisp abcl  '("abcl"))
(deflisp clisp '("clisp") nil nil (when (system-is-windows) ; set HOME variable on Windows
									(list (concat "HOME="
												  (file-name-as-directory
												   (expand-file-name (get-home-directory)))))))

;; Allegro
(deflisp allegro '("alisp"))
;; For using Allegro Common Lisp Express on Windows you should
;; create simple batch file named allegro-express-swank.bat and put.
;; Emacs should be able to start this file (put it in directory in Path environment variable).
;; We will try to create this file automatically otherwise.
;; Batch file contents:
;; @echo off
;; set /p swank_command="Enter the commands necessary to start the swank server on one line (slime will do this for you)"
;; echo Starting swank server
;; allegro-express +B +m -e "%swank_command%"
(defvar *allegro-express-launcher* nil) ; it is Windows specific file
(deflisp allegro-express (list
						  (if (and
							   (executable-exists-p "allegro-express")

							   (system-is-windows))
							  (if (executable-exists-p "allegro-express-swank")
								  "allegro-express-swank"
								(let ((tmp-file (make-temp-file "allegro-express-swank-" nil ".bat")))
								  ;; write data into temporary file
								  (with-temp-buffer
									(insert
									 (concat
									  "@echo off\r\n"
									  "set /p swank_command=\"Enter the commands necessary to start the swank server on one line (slime will do this for you)\"\r\n"
									  "echo Starting swank server\r\n"
									  "allegro-express +B +m -e \"%swank_command%\"\r\n"))
									(write-file tmp-file))
								  (add-hook 'kill-emacs-hook (lambda () ; remove temporary launcher file
															   (when (and (bound-and-true-p *allegro-express-launcher*)
																		  (file-exists-p *allegro-express-launcher*))
																 (delete-file *allegro-express-launcher*))))
								  (setq *allegro-express-launcher* tmp-file)))
							"allegro-express")))

(deflisp ecl '("ecl") nil nil (when (system-is-windows) ; set HOME variable on Windows
								(list (concat "HOME="
											  (file-name-as-directory
											   (expand-file-name (get-home-directory)))))))

(eval-after-load "slime"
  '(progn
	 (find-lisps)
	 (advice-add 'slime-init-command :around #'slime-init-command-advice)
	 ;; do not load failed fasl file
	 (setq slime-load-failed-fasl 'never)
	 ;; auto load SLIME
	 (defvar slime-autoloads nil)
	 (setq slime-autoloads 'always)
	 ;; enable evaluation in Emacs from SWANK for better Lisp->Emacs interaction
	 (setq slime-enable-evaluate-in-emacs t)
	 ;; SLIME autocompletion
	 (add-hook 'slime-mode-hook 'slime-company-maybe-enable)
	 (add-hook 'slime-repl-mode-hook 'slime-company-maybe-enable)
	 ))

;; prepare SLIME
(slime-setup
 ;; load SLIME extensions
 (setq slime-contribs +slime-contribs+))

;;; Hyperspec configuration ;;;
(defun hyperspec-eww-browse-url (url)
  "Browse Hyperspec URL in EWW."
  (switch-to-buffer-other-window (get-buffer-create "*hyperspec*"))
  (unless (eq major-mode 'eww-mode)
	(eww-mode))
  (setq-local shr-inhibit-images t)
  (cond
   ((http-url-p url)
	(eww-browse-url url))
   (t
	(eww-open-file url))))

(defun eww-advice (func &rest args)
  (when (and (eq (get-buffer "*hyperspec*")
				 (current-buffer))
			 (called-interactively-p))
	(bury-buffer))
  (apply func args))

(defun hyperspec-browse-url (url &optional window)
  "Browse Hyperspec URL."
  (hyperspec-eww-browse-url url))

(defun hyperspec-lookup-advice (func &rest args)
  ;; Open Hyperspec in EWW
  (let ((old-function browse-url-browser-function))
	(unwind-protect
		(progn
		  (setq-local browse-url-browser-function 'hyperspec-browse-url)
		  (apply func args))
	  (setq-local browse-url-browser-function old-function))))

(defun hyperspec-index ()
  "Open Hyperspec index."
  (interactive)
  (let ((hyperspec-index (cond
						  ((http-url-p common-lisp-hyperspec-root)
						   (if (string-ends-with common-lisp-hyperspec-root "/")
							   (concat common-lisp-hyperspec-root "Front/")
							 (concat common-lisp-hyperspec-root "/Front/")))
						  (t
						   (concat (file-name-as-directory common-lisp-hyperspec-root)
								   (file-name-as-directory "Front")
								   "index.htm")))))
	(hyperspec-browse-url hyperspec-index)))

(eval-after-load "eww"
  '(progn
	 (advice-add 'eww :around #'eww-advice)))

(eval-after-load "hyperspec"
  '(progn
	 ;; Local Hyperspec path
	 (let ((hs-path (getenv "CL_HYPERSPEC")))
	   (when hs-path
		 (setq common-lisp-hyperspec-root (expand-file-name (file-name-as-directory hs-path)))))
	 (advice-add 'hyperspec-lookup :around #'hyperspec-lookup-advice)))
