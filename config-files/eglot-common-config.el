;; common eglot configuration
(defun eglot-common-setup ()
  (company-mode 1)
  (eldoc-mode 1)
  (eglot-ensure)) ;; disable hints

(with-eval-after-load 'eglot
  (add-hook 'eglot-managed-mode-hook
            #'(lambda ()
                (when (fboundp 'eglot-inlay-hints-mode)
                  (eglot-inlay-hints-mode -1)))))

;; reconnect to LSP when files is being reloaded
;; (defun eglot-on-revert-callback ()
;;   (when (eglot-managed-p)
;;     (eglot-reconnect (eglot-current-server))))

;; (with-eval-after-load 'files
;;   (add-hook 'after-revert-hook #'eglot-on-revert-callback))
