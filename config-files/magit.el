;;;; Magit Configuration File

;; run pageant on Windows
(defun run-pageant ()
  (when (and (system-is-windows)
             (executable-exists-p "pageant")
             (getenv "GIT_SSH")
             (string-match "plink" (downcase (getenv "GIT_SSH"))))
    (unless (program-running-p "pageant")
      (let ((process (make-process :name "pageant"
                                   :buffer nil
                                   :command '("cmd.exe" "/c" "pageant")
                                   :connection-type nil)))
        (when (processp process)
          t)))))

(defun git-core-available-p ()
  (and (executable-exists-p "git-add")
       (executable-exists-p "git-blame")
       (executable-exists-p "git-tag")))

(defun find-windows-git-core ()
  (let ((git-core-path-reg (win-reg-read-64-or-32 "HKLM\\SOFTWARE\\GitForWindows" "LibexecPath")))
    (if git-core-path-reg
        (file-name-as-directory git-core-path-reg)
      (let ((git-install-path (expand-file-name (concat (file-name-directory (executable-find "git")) "../"))))
        (when git-install-path
          (let ((git-core32 (concat git-install-path "mingw32/libexec/git-core/"))
                (git-core64 (concat git-install-path "mingw64/libexec/git-core/")))
            (or (when (directory-exists-p git-core32) git-core32)
                (when (directory-exists-p git-core64) git-core64))))))))

;; Disable VC Git backend
(eval-after-load "vc"
  '(setq vc-handled-backends (delq 'Git vc-handled-backends)))

(eval-after-load "magit"
  '(progn
     ;; add path to git-core on Windows
     (when (system-is-windows)
       (unless (git-core-available-p)
         (let ((git-core-path (find-windows-git-core)))
           (when git-core-path
             (add-executable-path git-core-path t)))))
     (run-pageant)
     ;; Use manual pages for Git manual lookups
     (if (executable-exists-p "man")
         (setq magit-view-git-manual-method 'man)
       (setq magit-view-git-manual-method 'woman))
     ;; Create Git configuration file in the $HOME directory and
     ;; include global configuration file (on Windows only)
     (when (system-is-windows)
       (unless (file-exists-p "~/.gitconfig")
         (with-temp-buffer
           (insert
            (format
             "[include]\r\n    path = %s\r\n\r\n"
             (replace-regexp-in-string "\\\\" "\\\\\\\\"
                                       (emacs-path-to-system-path
                                        (concat
                                         (get-home-directory)
                                         ".gitconfig")))))
           (write-file "~/.gitconfig"))))))

(eval-after-load "transient"
  '(setq transient-default-level 5)) ;; enable signing, among other things

(defun run-magit-dispatch ()
  (interactive)
  (require 'magit)
  (magit-dispatch))

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'run-magit-dispatch)
