;; Patch Emacs's switch-to-buffer (and friends) to not choose other frames

(defvar *switching-buffer* nil)

(defun switch-to-buffer-advice (func &rest args)
  (let ((*switching-buffer* t))
    (apply func args)))

(defun get-buffer-window-advice (func &rest args)
  (if (not *switching-buffer*)
      (apply func args)
    (let ((buffer-or-name (car args)))
      (funcall func buffer-or-name nil))))

(advice-add 'get-buffer-window :around #'get-buffer-window-advice)

(with-eval-after-load 'window
  (advice-add 'switch-to-buffer :around #'switch-to-buffer-advice))

(with-eval-after-load 'ido
  (advice-add 'ido-switch-buffer :around #'switch-to-buffer-advice))
