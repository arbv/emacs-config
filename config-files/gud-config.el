;; A replacement for gdb--check-interpreter (to run Mozilla's RR).
(defun gdb--check-interpreter-replacement (filter proc string)
  (unless (zerop (length string))
    (remove-function (process-filter proc) #'gdb--check-interpreter)
    (funcall filter proc string)))

(defun gdb--check-interpreter-advice (func &rest args)
  (apply #'gdb--check-interpreter-replacement args))

(with-eval-after-load 'gdb-mi
  (advice-add 'gdb--check-interpreter :around #' gdb--check-interpreter-advice))

(with-eval-after-load 'gud
  ;; show many GDB windows
  (setq gdb-many-windows t)
  ;; show notification in gdb
  (gud-tooltip-mode t))
