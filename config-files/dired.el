;;; Dired configuration

;; keep state of dired omit mode
(defvar *dired-omit-mode-enabled* t)

(defun set-dired-omit-mode ()
  (if *dired-omit-mode-enabled*
	  (dired-omit-mode t)
	(dired-omit-mode -1)))

(defun dired-omit-switch ()
  "Switch dired omit mode."
  (interactive)
  (cond
   (*dired-omit-mode-enabled*
	(setq *dired-omit-mode-enabled* nil))
   (t
	(setq *dired-omit-mode-enabled* t)))
  (set-dired-omit-mode)
  (revert-buffer))

(defun open-seleceted-files-in-external-program ()
  "Open selected in dired files in default external program."
  (interactive)
  (when (major-mode-active-p "dired-mode")
	(let ((files (dired-get-marked-files)))
	  (when files
		(mapc #'open-file-in-external-program files)))))

;; Dired - sort directories first
;; from: http://www.emacswiki.org/emacs/DiredSortDirectoriesFirst

(defun dired-sort-directories-first ()
  "Sort dired listings with directories first."
  (save-excursion
	(let (buffer-read-only)
	  (forward-line 2) ;; beyond dir. header
	  (sort-regexp-fields t "^.*$" "[ ]*." (point) (point-max)))
	(set-buffer-modified-p nil)))

(defadvice dired-readin
  (after dired-after-updating-hook first () activate)
  "Sort dired listings with directories first before adding marks."
  (dired-sort-directories-first))

(eval-after-load "dired"
  '(progn
	 (require 'dired-x) ; require dired utilities
	 ;;(require 'dired-copy-paste) ; dired copy and paster support

	 ;; enable dwim
	 (setq dired-dwim-target t)
	 ;; set default dired ls switches
	 (setq dired-listing-switches "-lah")
	 (put 'dired-find-alternate-file 'disabled nil)
	 ;; Auto-refresh dired on file change
	 (add-hook 'dired-mode-hook 'auto-revert-mode)
	 ;; ignore dot-files by default
	 ;;(setq dired-omit-files (concat dired-omit-files "\\|^\\.+\w+.*$"))
	 (setq dired-omit-files "^\\.?#\\|^\\.[[:alnum:]]+.+$")
	 ;; omit dependency files
	 (add-to-list 'dired-omit-extensions ".d")
	 (add-hook 'dired-mode-hook #'set-dired-omit-mode)
	 (define-key dired-mode-map (kbd "C-x M-o") 'dired-omit-switch)

	 ;; bind open selected files in external program to Control+Enter
	 (define-key dired-mode-map [(control return)] 'open-seleceted-files-in-external-program)

	 ;; On Windows do not show group information in dired - it is almost useless
	 ;; in this OS.
	 (when (system-is-windows)
	   (setq ls-lisp-verbosity (delq 'gid ls-lisp-verbosity)))))

