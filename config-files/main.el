;; Require Common Lisp extensions and Emacs server
(eval-and-compile (require 'cl-lib)
				  (require 'server))

;;; Pickup themes from themes directory
(defconst +themes-path+ (concat user-emacs-directory "themes"))
;; user defined load path
(defconst +load-path-directory+ (file-name-as-directory (concat user-emacs-directory "load-path")))
(defconst +local-load-path-directory+ (file-name-as-directory (concat user-emacs-directory "local-load-path")))

;; load common utilities
(eval-and-compile
  (load-user-file "config-files/common"))

;; cleanup old empty session files
(defun arbv/remove-empty-session-files ()
  (arbv/loop-over-directory user-emacs-directory
                            #'(lambda (file-path)
                                (when (and (file-regular-p file-path)
                                           (zerop (file-attribute-size (file-attributes file-path)))
                                           (string-equal (file-name-base file-path) "session"))
                                  (delete-file file-path))
                                t)))

(arbv/remove-empty-session-files)

(defun server-file-belongs-to-emacs-p (&optional server-file-path)
  (emacs-pid-p (server-file-pid server-file-path) t))

(defun emacs-server-process-p (&optional server-file-path)
  (emacs-pid-p (server-file-pid server-file-path)))

;; Delete old server file if we need to.
(let ((server-file-path (expand-file-name server-name server-auth-dir)))
  (when (file-exists-p server-file-path)
    (unless (server-file-belongs-to-emacs-p server-file-path)
      (delete-file server-file-path))))

(when (server-running-p server-name)
  (setq server-name (concat server-name (format "%d" (emacs-pid)))))

;; start server
(unless (server-running-p server-name)
  (server-start)
  (if server-use-tcp
	  (setenv "EMACS_SERVER_FILE" (expand-file-name server-name server-auth-dir))
	(setenv "EMACS_SOCKET_NAME" server-name)))

(defun add-directory-to-load-path (dirname)
  "Adds a directory and its subdirectories to the LOAD-PATH."
  (when (file-directory-p dirname)
	(add-to-list 'load-path dirname)
	(let ((files (directory-files dirname)))
	  (dolist (f files)
		(let ((path (concat (file-name-as-directory dirname) f)))
		  (when (and (file-directory-p path)
					 (not (string= f "."))
					 (not (string= f "..")))
			(add-to-list 'load-path path)))))))

;; add profile based load paths
(add-directory-to-load-path +load-path-directory+)
(add-directory-to-load-path +local-load-path-directory+)
;; recompile files in load path
(byte-recompile-directory (expand-file-name +load-path-directory+) 0)
(byte-recompile-directory (expand-file-name +local-load-path-directory+) 0)

(when (directory-exists-p +themes-path+)
  (dolist
	  (theme-dir (directory-files +themes-path+))
	(when (not
		   (or (string= theme-dir ".")
			   (string= theme-dir "..")))
	  (let* ((theme-path (file-name-as-directory (concat +themes-path+ "/" theme-dir))))
		(when (directory-exists-p theme-path)
		  (byte-recompile-directory (expand-file-name theme-path) 0) ; compile theme files
		  (add-to-list 'custom-theme-load-path theme-path)
		  (add-to-list 'load-path theme-path))))))

;; MSYS on Windows
(when (system-is-windows)
  (load-user-file "config-files/msys"))

;; configuration for TeXinfo
(load-user-file "config-files/info")

;; load and configure packages
(load-user-file "config-files/packages")
;; general configuration
(load-user-file "config-files/general")
(load-user-file "config-files/switch-to-buffer-patchup")
;; Elisp configuration
(load-user-file "config-files/elisp-config")
;; project.el
(load-user-file "config-files/project-config")
(load-user-file "config-files/compile-config")
(load-user-file "config-files/editorconfig-common")
(load-user-file "config-files/eglot-common-config")
;; FlyCheck and FlyMake
(load-user-file "config-files/flycheck-config")
(load-user-file "config-files/flymake-config")
;; company - auto-completion back-end
(load-user-file "config-files/company-config")
;; EShell configuration
(load-user-file "config-files/eshell-config")
;; dired setup
(load-user-file "config-files/dired")
;; gui setup
(load-user-file "config-files/gui")
;; user
(load-user-file "user")
;; load SLIME configuration
(load-user-file "config-files/slime-setup")
;; spelling configuration
(load-user-file "config-files/spelling")
;; Code-Browsing
(load-user-file "config-files/code-browsing-config")
;; load bookmark+ config file
;(load-user-file "config-files/bookmark+")
;; auctex
(when (executable-exists-p "tex")
  (load-user-file "config-files/auctex-config"))
;; Load GUD (Debugger) configuration file
(load-user-file "config-files/gud-config")
;; Rust mode
(load-user-file "config-files/rust-config")
;; rcirc setup
(load-user-file "config-files/rcirc-config")
;; Lua support
(load-user-file "config-files/lua-config")
;; Ruby support
(load-user-file "config-files/ruby-config")
;; Python support
(load-user-file "config-files/python-config")
;; Org-mode
(load-user-file "config-files/org-config")
;; C and C++
(load-user-file "config-files/clang-format-config")
(load-user-file "config-files/c-cpp-irony")
(load-user-file "config-files/c-cpp-eglot") ; eglot + clangd
(load-user-file "config-files/c-cpp-common")
(load-user-file "config-files/arduino")
;; F#
;; (when (executable-find "dotnet")
;;   (load-user-file "config-files/fsharp-config"))
;; Haskell
(load-user-file "config-files/haskell-config")
;; Git
(when (and (> emacs-major-version 25) (executable-exists-p "git"))
  (load-user-file "config-files/magit"))
;; shell files editing
(load-user-file "config-files/shell-mode")
;; Nix
(when (executable-exists-p "nix")
  (load-user-file "config-files/nix-config"))
;; load desktop
(load-user-file "config-files/desktop-config")
;; we will load this file last for testing purposes
(load-user-file "config-files/test")
