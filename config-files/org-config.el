;; !!! org mode configuration !!!
(defconst +org-skeleton-path+ (expand-file-name (file-name-as-directory
                                                 (concat user-emacs-directory
                                                         (file-name-as-directory "misc")
                                                         "org-skeleton"))))

(defvar *org-files-path* (file-name-as-directory
                          (if (getenv "ORG_FILES")
                              (system-path-to-emacs-path (getenv "ORG_FILES"))
                            (concat (get-home-directory) "org"))))

(defvar *org-wiki-path* (file-name-as-directory
                              (concat *org-files-path* "wiki")))

(defvar *org-wiki-index* (concat *org-wiki-path* "index.org"))

(defvar *org-default-tasks-file* (concat *org-files-path* "_tasks.org"))

(defvar *org-someday-tasks-file* (concat *org-files-path* "_someday.org"))

(defvar *org-notes-file* (concat *org-files-path* "notes.org"))

(defvar *org-custom-config-file* (concat *org-files-path* "config.el"))


(defun initialize-org-directory (&optional force)
  (when (and (not (directory-exists-p *org-files-path*))
             force)
    (make-directory *org-files-path*))
  (when (directory-empty-p *org-files-path*)
    (copy-directory +org-skeleton-path+ *org-files-path*
                    nil nil t)))

;; some handy commands
(defun dired-org ()
  "Open ORG-DIRECTORY in Dired."
  (interactive)
  (initialize-org-directory t)
  (dired *org-files-path*))

(defun find-org-file (path)
  (initialize-org-directory t)
  (if (file-exists-p path)
      (find-file path)))

(defun tasks ()
  "Open default org-mode tasks file."
  (interactive)
  (find-org-file *org-default-tasks-file*))

(defun notes ()
  "Open default org-mode notes file."
  (interactive)
  (find-org-file *org-notes-file*))

(defun someday ()
  "Open someday org-mode tasks file."
  (interactive)
  (find-org-file *org-someday-tasks-file*))

(defun wiki ()
  "Open org-mode wiki index file."
  (interactive)
  (find-org-file *org-wiki-index*))

(defun org-init ()
  "Initializes Org-mode directory."
  (interactive)
  (initialize-org-directory t))

;; load custom org-mode configuration
(defun org-reconfigure ()
  (interactive)
  "Reload local org-mode configuration file."
  (when (file-exists-p *org-custom-config-file*)
    (load *org-custom-config-file*)))

;; org export setup
(defun add-org-export-translation (word lang new-translation)
  "Update translation for the word used in org-mode exporter."
  (let ((word-translations (assoc word org-export-dictionary)))
    (when word-translations
      (let ((translation (assoc lang (cdr word-translations))))
        (if translation
            (setcdr translation new-translation)
          (setcdr word-translations
                  (cons (cons lang new-translation)
                        (cdr word-translations))))))))

(eval-after-load "org"
  '(progn
     (dolist (m '(ox-odt ox-bibtex))
       (add-to-list 'org-modules m t))
     (org-load-modules-maybe)
     (setq org-directory *org-files-path*)
     (setq org-default-notes-file (file-name-nondirectory *org-notes-file*))

     ;;(initialize-org-directory)

     ;; configure agenda files
     ;; Agenda files should have names with '_' as  first characters.
     (setq org-agenda-file-regexp "\\`_[^.].*\\.org\\'")
     (add-to-list 'org-agenda-files
                  *org-files-path*)

     ;; usual RefTeX citation
     (define-key org-mode-map (kbd "C-c [") 'org-reftex-citation)
     ;; use latexmk to produce PDF
     ;;(when (executable-exists-p "latexmk")
     ;;  (setq org-latex-pdf-process '("latexmk -bibtex -interaction=nonstopmode -file-line-error -synctex=1 -pdf -f %f")))
     ;; Do not truncate lines by default.
     (add-hook 'org-mode-hook (lambda ()
                                (setq truncate-lines nil) ;truncation disabled
                                (visual-line-mode t)))

     ;; Enter follows link
     (setq org-return-follows-link t)
     ;; enable IDO
     (setq org-completion-use-ido t)
     ;; custom date and time format
                                        ;(setq org-time-stamp-custom-formats '("<%a %m.%d.%Y>" . "<%m.%d.%Y %a %H:%M>"))
                                        ;(setq-default org-display-custom-times t)
     ;; mark done tasks with time mark
     (setq org-log-done 'time)
     ;; show all contents of the file by default
     (setq org-startup-folded 'showall)
     ;; use fast todo selection
     (setq org-use-fast-todo-selection t)
     ;; set additional TODO keywords
     (setq org-todo-keywords '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "|"
                                         "DONE(d)" "CANCELED(c)" "DEFERRED(f)" "SOMEDAY(m)")))
     ;; Task priorities - the Eisenhower's Matrix:
     ;; #A - important and urgent (do);
     ;; #B - important and not urgent (plan);
     ;; #C - not important and urgent (delegate - routine work);
     ;; #D - not important and not urgent (eliminate);
     (setq org-highest-priority ?A)
     (setq org-lowest-priority  ?D)

     ;; configure default tags
     (setq org-tag-alist '(("work" . ?w)
                           ("home" . ?h)
                           ("personal" . ?m) ; m - me
                           ("project" . ?p)
                           ("reading" . ?r)))

     ;; default capture templates
     (setq org-capture-templates
           '(("t" "TODO" entry (file+headline "_tasks.org" "Tasks")
              "* TODO %?\n %i\n")
             ("s" "Someday Task" entry (file+headline "_someday.org" "Someday Tasks")
              "* SOMEDAY %?\n %i\n%t\n")
             ("n" "Note" entry (file+headline "notes.org" "Notes")
              "* %?\n %i\n")))

     ;; set refile targets
     (setq org-refile-targets '((nil :maxlevel . 3)
                                (org-agenda-files :maxlevel . 3)
                                (*org-notes-file* :maxlevel . 3)))

     (setq org-outline-path-complete-in-steps t) ; disable IDO for refilling
     (setq org-refile-use-outline-path 'file) ; Show full paths for refiling

     ;; disable exported code blocks fontification because it depends on the current theme
     ;; Alternative solution: https://github.com/legoscia/dotemacs/blob/master/dotemacs.org#theme-for-org-html-export
     (setq org-html-htmlize-output-type nil)

     ;; Support for References translation in Russian and Ukrainian
     (add-org-export-translation "References" "ru" '(:utf-8 "Источники" :default "Источники" :html "&#x418;&#x441;&#x442;&#x43E;&#x447;&#x43D;&#x438;&#x43A;&#x438;"))
     (add-org-export-translation "References" "uk" '(:utf-8 "Джерела" :default "Джерела" :html "&#x414;&#x436;&#x435;&#x440;&#x435;&#x43B;&#x430;"))
     (setq org-html-xml-declaration (cons (cons "html" "")
                                          (remove (assoc-string "html" org-html-xml-declaration)
                                                  org-html-xml-declaration)))
     (org-reconfigure)))

;; to fix org-store-link after Emacs startup
(eval-after-load "ol"
  '(require 'org))

(autoload 'org-publish "ox" nil t) ; to make org-publish accessible after Emacs startup

;; set some global org mode keys
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(define-key global-map "\C-cc" 'org-capture)
(define-key global-map "\C-cw" 'wiki)

(add-hook 'emacs-startup-hook #'org-init)

;; Deft - search in org mode files
(eval-after-load "deft"
  '(progn
     ;; deft configuration (search in wiki-org files)
     (setq deft-extensions '("org" "txt"))
     (setq deft-directory *org-wiki-path*)
     (setq deft-recursive t)))

;; key binding for searching in wiki
(global-set-key (kbd "C-c C-g") 'deft)
