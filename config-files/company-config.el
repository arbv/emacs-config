;;; company auto-completion back-end configuration

(defun company-mode-config ()
  (let ((quickhelp 1))
    ;; do not show tooltip automatically in comint and derived modes
    (when (derived-mode-p 'comint-mode)
      (setq quickhelp -1)
      (setq-local company-idle-delay nil))
    (company-quickhelp-mode quickhelp)
    ;; rebind standard completion keys
    (local-set-key (kbd "C-M-i") #'company-complete)
    (unless (or (derived-mode-p 'prog-mode)
                (derived-mode-p 'text-mode))
      (local-set-key (kbd "<tab>") #'company-complete))))

(with-eval-after-load 'company
  (setq company-tooltip-align-annotations t)
  (setq company-idle-delay 0.2)
  (setq company-minimum-prefix-length 1)
  (add-hook 'company-mode-hook #'company-mode-config))

(with-eval-after-load 'comint
  (add-hook 'comint-mode-hook #'company-mode))
