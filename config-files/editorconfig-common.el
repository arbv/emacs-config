(defconst +editorconfig-default-filename+ ".editorconfig")

(defun local/editorconfig-enable-if-available ()
  (let ((buffer-file-name (buffer-file-name)))
    (when (and buffer-file-name
               (locate-dominating-file buffer-file-name +editorconfig-default-filename+))
      (editorconfig-mode 1))))

(with-eval-after-load 'prog-mode
  (add-hook 'prog-mode-hook 'local/editorconfig-enable-if-available))
