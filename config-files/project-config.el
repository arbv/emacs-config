;; Patch VC to make project.el work nicely with Magit installed and
;; Git being removed from VC backend.
(defun project-vc-advice(func &rest args)
  (let ((vc-handled-backends (if (member 'Git vc-handled-backends)
                                 vc-handled-backends
                               (cons 'Git vc-handled-backends))))
    (apply func args)))

(with-eval-after-load 'project
  (require 'vc)
  (advice-add 'project-current :around #'project-vc-advice)
  (advice-add 'project-roots :around #'project-vc-advice)
  (advice-add 'project-files :around #'project-vc-advice)
  (advice-add 'project-external-roots :around #'project-vc-advice)
  (advice-add 'project-ignores :around #'project-vc-advice))
