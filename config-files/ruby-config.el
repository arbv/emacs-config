;; configuration file for Ruby support in Emacs
;; You should have following gems installed
;; - pry
;; - pry-doc
;; - method_source
;;
;; Note that gems for current version of ruby should be accessible from PATH
;;

(eval-after-load "ruby-mode"
  '(progn
	 ;; enable syntax checking for Ruby
	 (add-hook 'ruby-mode-hook
			   (lambda ()
				 (flycheck-mode 1)
				 ;; go to next/previous flycheck error error
				 (local-set-key (kbd "M-n") #'flycheck-next-error)
				 (local-set-key (kbd "M-p") #'flycheck-previous-error)))
	 ;; key bindings
	 (add-hook 'ruby-mode-hook
			   (lambda ()
				 (local-set-key (kbd "C-c C-c") 'ruby-send-definition) ;; send definition to the interpreter
				 (local-set-key (kbd "C-c C-r") 'ruby-send-region)     ;; send current region to the interpreter
				 (local-set-key (kbd "C-c M-l") 'ruby-send-buffer)))   ;; send current buffer to the interpreter
	 ;; enable Robe for Ruby files
	 (add-hook 'ruby-mode-hook 'robe-mode)
	 ))

(eval-after-load "robe"
  '(progn
	 (add-hook 'robe-mode-hook 'eldoc-mode)
	 ;; enable auto-completion
	 ;; (pushnew 'company-robe company-backends)
	 ;; (add-hook 'robe-mode-hook (lambda ()
	 ;;								(setq company-tooltip-align-annotations t)
	 ;;								(setq company-idle-delay 0.2)
	 ;;								(setq company-minimum-prefix-length 1)
	 ;;								(setq company-selection-wrap-around t)
	 ;;								(local-set-key (kbd "Tab") 'company-select-next)
	 ;;								(company-mode 1)))
	 (add-hook 'robe-mode-hook 'ac-robe-setup)
	 ))

(eval-after-load "inf-ruby"
  '(progn
	 ;; enable robe mode for console buffer
	 (add-hook 'inf-ruby-mode-hook 'robe-mode)))

