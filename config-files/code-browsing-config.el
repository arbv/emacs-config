(defconst +code-browsing-window-width+ 35)

;; Neotree for code browsing

(defun neotree/toggle-local ()
  (interactive)
  (if (and (fboundp 'neo-global--window-exists-p)
           (neo-global--window-exists-p))
      (neotree-hide)
    (let ((file-name (buffer-file-name)))
      (neotree-hide) ;; hack to autoload neotree
      (when neo-smart-open
        (let ((current-project (project-current)))
          (when current-project
            (neotree-dir (project-root current-project))))
        (neotree-find file-name))
      (let ((neo-smart-open nil))
        (neotree-show)))))

(global-set-key (kbd "<f8>") 'neotree/toggle-local)

(with-eval-after-load 'neotree
  (setq neo-smart-open t)
  (setq neo-window-width +code-browsing-window-width+))

(defun imenu-list/eglot-toggle ()
  (interactive)
  (imenu-list-smart-toggle))

;; imenu-list to browse tags
(global-set-key (kbd "<f9>") 'imenu-list/eglot-toggle)

(with-eval-after-load 'imenu-list
  ;; automatically focus on the tags list on activation
  (setq imenu-list-size +code-browsing-window-width+)
  (setq imenu-list-focus-after-activation t))

(with-eval-after-load 'imenu
  ;; enable tags auto rescan
  (setq imenu-auto-rescan t)
  ;; sort tags by name
  (setq imenu-sort-function 'imenu--sort-by-name))
