;; Arduino programming support
(defun get-arduino-tools-directory-w32 ()
  (let ((arduino-dir (win-reg-read-64-or-32 "HKEY_LOCAL_MACHINE\\SOFTWARE\\Arduino" "Install_Dir")))
	(when arduino-dir
	  (system-path-to-emacs-path arduino-dir))))

(when (system-is-windows)
  (let ((arduino-dir (get-arduino-tools-directory-w32)))
	(when arduino-dir
	  (setenv "ARDUINODIR" arduino-dir))))

(defun get-arduino-sketch-name ()
  (when default-directory
	(let* ((file-name (concat (file-name-nondirectory (directory-file-name default-directory)) ".ino"))
		   (full-path (concat default-directory file-name))
		   (bfn (buffer-file-name)))
	  (or (and (string-equal bfn
							 full-path)
			   full-path)
		  (and full-path
			   (file-exists-p full-path)
			   full-path)))))

(defun get-arduino-sketch-compile-command (&optional command)
  (concat
   (if (system-is-windows)
       "sh -c \""
     "")
   (if (system-is-windows)
       "SERIALDEV=COM1"
     "SERIALDEV=/dev/ttyUSB0")
   " "
   "BOARD=arduino:avr:uno"
   " "
   "VERBOSE=0"
   " "
   "make -f "
   (if (system-is-windows)
       "\\\""
     "\"")
   (concat (expand-file-name user-emacs-directory)
           "misc/arduino/arduino-avr.mk")
   (if (system-is-windows)
       "\\\""
     "\"")
   " "
   (if command
	   command
	 "upload")
   (if (system-is-windows)
       "\""
     "")))

;; (defun arduino-browse-documentation ()
;;   (interactive)
;;   (browse-url
;;    (concat "https://www.arduino.cc/en/Reference/"
;; 		   (thing-at-point 'symbol))))

(defun arduino-clang-compile-options ()
  (split-string
   (shell-command-to-string (get-arduino-sketch-compile-command "clang-complete"))
   "\n"
   t))

(defun arduino-compile-hook ()
  (let ((buffer-file-name (buffer-file-name)))
	(when (or (get-arduino-sketch-name)
			  (and buffer-file-name
				   (string-equal (file-name-extension buffer-file-name) "ino")))
	  (let ((sketch-command (get-arduino-sketch-compile-command)))
		(when sketch-command
		  (set (make-local-variable 'compile-command)
			   sketch-command))))))

(defun arduino-irony-mode-adjust-options ()
  (let ((buffer-file-name (buffer-file-name)))
	(when (or (get-arduino-sketch-name)
			  (and buffer-file-name
				   (string-equal (file-name-extension buffer-file-name) "ino")))
	  (let ((clang-compile-options (arduino-clang-compile-options)))
		(when clang-compile-options
		  (setq-local irony-additional-clang-options
					  (arduino-clang-compile-options)))))))

(defun arduino-disable-flycheck ()
  (let ((buffer-file-name (buffer-file-name)))
	(when (or (get-arduino-sketch-name)
			  (and buffer-file-name
				   (string-equal (file-name-extension buffer-file-name) "ino")))
	  (flycheck-mode -1))))

(eval-after-load "irony"
  '(progn
	 (add-hook 'irony-mode-hook  #'arduino-compile-hook)
	 (add-hook 'irony-mode-hook  #'arduino-irony-mode-adjust-options)
	 ;;(add-hook 'irony-mode-hook  #'arduino-disable-flycheck t)
	 ))

(add-to-list 'auto-mode-alist '("\\.ino\\'" . c++-mode))
