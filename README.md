# Emacs Configuration

## Intro

This is my configuration for Emacs. I started using Emacs after I
started learning the Common Lisp programming language, so it includes
configuration for SLIME and extensive Common Lisp implementation
detection facilities. It is only needed for a Common Lisp
implementation to be in `PATH`. It also contains sane configuration
for `Dired`.

I am being using this config daily on both Linux and Windows and I do
not see any difficulties to using it on OS X or any other Unix-like
operating system.

I tried to keep it modular and clear. After you start Emacs first time
it will automatically download all the needed packages.  You can
change list of automatically downloadable packages in `packages.el`.

I hope you will find it useful and learn some tricks from it.

## Email and user name setup

I suggest you to create the `user.el` file with your user name and
e-mail. It will look like:

<pre>
;; setup username and e-mail
(setq user-full-name "Name Surname")
(setq user-mail-address "your.email@example.com")
</pre>

## Notes

I added some commands to simplify experimenting with Emacs
configuration.

- `reload` - it will reload `init.el` file. It results in rereading
all the configuration files by Emacs. Works *most* of the time. If you
have some troubles after executing this command, just restart Emacs.
- `dired-config` - it will open Emacs configuration directory
(`~/.emacs.d/`) in `Dired`.
- `dired-home` - open home directory in `Dired`.

## License

All the files in this configuration are in public domain if not stated
otherwise.
