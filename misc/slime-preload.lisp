;;; This is a file loaded on initialization by the SLIME right before the creation of the SWANK package.

;; You might put some code here to load in almost every detected Lisp system
;; on SLIME start.
;;
;; There are two constants defined during load of this file:
;; +SLIME-USER-FULL-NAME+ - it contains value of the Emacs variable USER-FULL-NAME;
;; +SLIME-USER-MAIL-ADDRESS+ - it contains value of the Emacs variable USER-MAIL-ADDRESS.
;;
;; These two variables used to load and initialize QuickProject if it was not loaded by the lisp implementation.

;; Try to load QuickLisp
#-quicklisp
(ignore-errors
  (let ((quicklisp-init (merge-pathnames
                         "quicklisp/setup.lisp"
                         (user-homedir-pathname))))
    (when (probe-file quicklisp-init)
      (load quicklisp-init))))

(defun %set-variable-in-package (package-name variable-name value)
  "Set value in package."
  (setf (symbol-value (read-from-string (format nil "~A:~A"
                                                package-name
                                                variable-name)))
        value))

;; Load QuickProject if available and not loaded.
#+asdf
(ignore-errors
  
  (when (and (asdf:find-system :quickproject)
             (not (find-package :quickproject)))
    (asdf:operate 'asdf:load-op 'quickproject)
    ;; set user name and email
    (when (and (plusp (length +slime-user-full-name+))
               (plusp (length +slime-user-mail-address+ )))
      (%set-variable-in-package "quickproject"
                                "*author*"
                                (format nil "~A <~A>"
                                        +slime-user-full-name+
                                        +slime-user-mail-address+)))
    ;; set default license to MIT
    (%set-variable-in-package "quickproject"
                              "*license*"
                              "MIT")))

;; resident editor support
(defun %edit-in-emacs (&optional what)
  ;; One need to do it hard way because SWANK package was not created yet.
  (funcall (read-from-string
            "swank:ed-in-emacs") what))
#+ccl
(progn
  (unless ccl:*resident-editor-hook*
    (setf ccl:*resident-editor-hook* #'%edit-in-emacs)))
#+sbcl
(progn
  (unless sb-ext:*ed-functions*
    (push (lambda (&rest args)
            (apply #'%edit-in-emacs args)
            t)
          sb-ext:*ed-functions*)))
#+allegro ; wrap ED function
(progn
  (def-fwrapper %allegro-ed-wrapper (&optional what)
    (%edit-in-emacs what))
  (fwrap 'ed '%ed-wrapper '%allegro-ed-wrapper))
#+abcl ; Almost alike SBCL, see above.
(progn
  (when (and (eql (length *ed-functions*) 1)
             (not (member #'%edit-in-emacs *ed-functions*)))
    (push (lambda (&rest args)
            (apply #'%edit-in-emacs args)
            t)
          *ed-functions*)))

;; some development utilities
(defpackage :dev
  (:use cl))

(in-package :dev)

(defmacro export-from-dev (symbol)
  "Export symbol from DEV package."
  `(export ,symbol (find-package :dev)))

#+quicklisp
(defun make-project-path (name)
  "Create pathname for the project named NAME in the Quicklisp 'local-projects' directory."
  (check-type name string)
  (unless (search ".." name)
    (let ((local-projects-dir (car ql:*local-project-directories*)))
      (check-type local-projects-dir pathname)
      (make-pathname :host (pathname-host local-projects-dir)
                     :device (pathname-device local-projects-dir)
                     :directory (concatenate 'list (pathname-directory local-projects-dir) (list name))
                     :version :newest))))

#+quicklisp
(export-from-dev 'make-project-path)

#+quicklisp
(defun list-projects ()
  "List Quicklisp local project directories in alphabetic order."
  (mapc #'(lambda (name)
            (format t "~A~%" name))
        (sort (remove-duplicates (mapcar #'(lambda (e)
                                             (car (last (pathname-directory e))))
                                         (ql:list-local-projects)) :test #'equal) #'string-lessp))
  (values))

#+quicklisp
(export-from-dev 'list-projects)

#+(and quicklisp asdf3) ; ASDF 3 contains UIOP, we need former for DELETE-DIRECTORY-TREE
(defun delete-project (name &key force)
  "Delete local project NAME. Do not ask user confirmation when :FORCE is true."
  (labels ((ask-user (msg)
             (format t "~A~%(Y/N):~%" msg)
             (let ((answer (read)))
               (cond
                 ((eq answer
                      (read-from-string "Y")) t)
                 ((eq answer
                      (read-from-string "N")) nil)
                 (t
                  (ask-user msg))))))
    (check-type name string)
    (let* ((local-projects-dir (car ql:*local-project-directories*))
           (project-path (make-pathname :host (pathname-host local-projects-dir)
                                        :device (pathname-device local-projects-dir)
                                        :directory (concatenate 'list (pathname-directory local-projects-dir) (list name))
                                        :version :newest)))
      (check-type project-path pathname)
      ;; sanity check to avoid accidental deletion of the whole projects directory
      (unless  (or  (zerop (length name))
                    (search ".." name)
                    (wild-pathname-p project-path)
                    (equal project-path local-projects-dir)
                    (not (equal (merge-pathnames
                                 (uiop:subpathp project-path
                                                local-projects-dir)
                                 local-projects-dir)
                                project-path)))
        (when (and (probe-file project-path)
                   (or force
                       (ask-user (format nil "Do you really want to delete a project ~S (~A)?" name project-path))))
          (uiop:delete-directory-tree project-path :validate t :if-does-not-exist :ignore)
          (ql:register-local-projects)
          t)))))

#+(and quicklisp asdf3)
(export-from-dev 'delete-project)

#+quicklisp
(defun open-project-directory (name)
  "Open local project directory in Dired."
  (check-type name string)
  (let ((project-path (dev:make-project-path name)))
    (check-type project-path pathname)
    ;; sanity check to avoid accidental deletion of the whole projects directory
    (when (probe-file project-path)
      (funcall (read-from-string "swank:eval-in-emacs")
               `(dired ,(format nil "~A" project-path)))
      t)))

#+quicklisp
(export-from-dev 'open-project-directory)

;; Load SBCL code coverage support.
;; use "(declaim (optimize sb-cover:store-coverage-data))" to enable instrumentation,
;; (declaim (optimize (sb-cover:store-coverage-data 0))) to disable instrumentation.
;; To generate HTML report, run: (sb-cover:report "directory/")
#+sbcl
(require "sb-cover")

