### user defined rules
# here a user can define custom rules (if necessary), or define additional double colon rules:
#all::
#clean::
#cleanall::
#install::
#uninstall::
# (See the following link for information about double colon rules:
#  https://www.gnu.org/software/make/manual/html_node/Double_002dColon.html)

