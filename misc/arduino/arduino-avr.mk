# Partially based on http://ed.am/dev/make/arduino-mk/arduino.mk
# Heavily relies on arduino-builder.
# Works on Windows and Linux

# arduino directory path
ifndef ARDUINODIR
ARDUINODIR=/usr/share/arduino
ARDUINO_BUILDER ?= arduino-builder
else
ARDUINO_BUILDER ?= "$(ARDUINODIR)/arduino-builder"
endif

# board name
BOARD ?= arduino:avr:uno

BOARD_MANUFACTURER = $(shell echo $(BOARD) | cut -d ':' -f 1)
BOARD_PLATFORM = $(shell echo $(BOARD) | cut -d ':' -f 2)
BOARD_ID = $(shell echo $(BOARD) | cut -d ':' -f 3)

ifdef WINDIR
SERIALDEV ?= COM1
else
SERIALDEV ?= /dev/ttyUSB0
endif

# variables
PWD = "$(shell pwd)"
DIRNAME = $(shell basename ${PWD})
MAIN_INOFILE = $(shell basename ${DIRNAME}).ino
ifeq "$(BOARD_PLATFORM)" "avr"
OUT = "$(MAIN_INOFILE).hex" "$(MAIN_INOFILE).with_bootloader.hex"
endif
ifeq "$(BOARD_PLATFORM)" "sam"
OUT = "$(MAIN_INOFILE).bin"
endif
ifeq "$(BOARD_PLATFORM)" "msp430"
OUT = "$(MAIN_INOFILE).hex"
endif

findpath = $(shell if [ -d $(1) ] || [ -f $(1) ] ; then \
                       echo $(1); \
                    elif [ -d $(2) ] || [ -f $(2) ] ; then \
                       echo $(2); \
                    elif [ -d $(3) ] || [ -f $(3) ] ; then \
                       echo $(3); \
                    elif [ -d $(4) ] || [ -f $(4) ] ; then \
                       echo $(4); \
                    elif [ -d $(5) ] || [ -f $(5) ] ; then \
                       echo $(5); \
                    elif [ -d $(6) ] || [ -f $(6) ] ; then \
                       echo $(6); \
                    elif [ -d $(7) ] || [ -f $(7) ] ; then \
                       echo $(7); \
                    elif [ -d $(8) ] || [ -f $(8) ] ; then \
                       echo $(8); \
                    elif [ -d $(9) ] || [ -f $(9) ] ; then \
                       echo $(9); \
                    fi)

ARDUINO_HARDWARE_TOOLS = $(call findpath,"$(ARDUINODIR)/hardware/tools","$(ARDUINODIR)/hardware/archlinux-arduino")

# builder configuration
ARDUINO_BUILDER_ARGS = -hardware "$(ARDUINODIR)/hardware" -tools "$(ARDUINO_HARDWARE_TOOLS)" -tools "$(ARDUINODIR)/tools-builder" -warnings all -fqbn $(BOARD)

ARDUINO_ARCH_TOOLS = $(call findpath,"$(ARDUINO_HARDWARE_TOOLS)/$(BOARD_PLATFORM)")
ifneq "$(ARDUINO_ARCH_TOOLS)" ""
ARDUINO_BUILDER_ARGS += -tools "$(ARDUINO_ARCH_TOOLS)"
endif

ARDUINO_LIBRARIES = $(call findpath,"$(ARDUINODIR)/libraries")
ifneq "$(ARDUINO_LIBRARIES)" ""
ARDUINO_BUILDER_ARGS += -libraries "$(ARDUINO_LIBRARIES)"
endif

unixpath = $(shell echo $(1) | sed -e 's/\\/\//g')

ifeq "$(BOARD_MANUFACTURER)" "energia"
ARDUINO_USERDIR = $(call unixpath,'$(call findpath,\
 "$(APPDATA)\\..\\Local\\energia15",\
 "$(HOME)/.energia15",\
 "$(HOME)/Library/Energia15")')
else
ARDUINO_USERDIR = $(call unixpath,'$(call findpath,\
 "$(APPDATA)\\..\\Local\\Arduino15",\
 "$(HOME)/.arduino15",\
 "$(HOME)/Library/Arduino15",\
 "$(APPDATA)\\..\\Local\\Arduino",\
 "$(HOME)/.arduino",\
 "$(HOME)/Library/Arduino")')
endif

ifneq "$(ARDUINO_USERDIR)" ""
# local platforms and tools
ARDUINO_USER_PACKAGES = $(call findpath,"$(ARDUINO_USERDIR)/packages")

ifneq "$(ARDUINO_USER_PACKAGES)" ""
ARDUINO_BUILDER_ARGS += -hardware "$(ARDUINO_USER_PACKAGES)" -tools "$(ARDUINO_USER_PACKAGES)"
endif

# local libraries
PREFERENCES_FILE = $(call findpath,$(ARDUINO_USERDIR)/preferences.txt)

ifneq "$(PREFERENCES_FILE)" ""
SKETCHBOOK_DIR = $(call unixpath,'$(shell sed -ne "s/sketchbook.path=\(.*\)/\1/p" "$(PREFERENCES_FILE)")')
ifneq "$(SKETCHBOOK_DIR)" ""
ARDUINO_BUILDER_ARGS += -libraries "$(SKETCHBOOK_DIR)/libraries"
endif
endif
endif

# board configuration
# boardparams =  $(shell $(ARDUINO_BUILDER) $(ARDUINO_BUILDER_ARGS) -dump-prefs $(MAIN_INOFILE) > "/tmp/$(MAIN_INOFILE).build_params.txt" )
# readboardsparam = $(shell echo -n $(boardparams) ; sed -ne "s/^$(1)=\(.*\)/\1/p" < "/tmp/$(MAIN_INOFILE).build_params.txt")
readboardsparam_direct = $(shell $(ARDUINO_BUILDER) $(ARDUINO_BUILDER_ARGS) -dump-prefs $(MAIN_INOFILE) | sed -ne "s/^$(1)=\(.*\)/\1/p")
OUTDIR := "$(call unixpath,'$(call readboardsparam_direct,build.path)')"
BOARD_PREFS_FILE := "$(OUTDIR)/$(MAIN_INOFILE).board_prefs.txt"
BOARD_PREFS := $(shell mkdir -p $(OUTDIR); $(ARDUINO_BUILDER) $(ARDUINO_BUILDER_ARGS) -dump-prefs $(MAIN_INOFILE) > $(BOARD_PREFS_FILE))
readboardsparam = $(shell  sed -ne "s/^$(1)=\(.*\)/\1/p" $(BOARD_PREFS_FILE))
PLATFORMDIR =  $(call unixpath,'$(call readboardsparam,runtime.platform.path)')
BOARDS := "$(PLATFORMDIR)/boards.txt"
PROGRAMMERS := "$(PLATFORMDIR)/programmers.txt"
# detect upload tool
UPLOAD_TOOL_NAME := $(call readboardsparam,upload.tool)

CORE_PATH :=  $(call unixpath,"$(call readboardsparam,build.core.path)")
VARIANT_PATH :=  $(call unixpath,"$(call readboardsparam,build.variant.path)")

C_COMPILER := $(call unixpath,"$(call readboardsparam,compiler.c.cmd)")
BINUTILS_ARCH = $(shell $(C_COMPILER) -dumpmachine)
BINUTILS_PATH := $(call unixpath,"$(call readboardsparam,runtime.tools.$(C_COMPILER).path)")
BINUTILS_INCLUDE = $(call findpath,"$(BINUTILS_PATH)/$(BINUTILS_ARCH)/include")

# common boards options
BOARD_NAME := $(call readboardsparam,name)
BOARD_BUILD_ARCH := $(call readboardsparam,build.arch)
BOARD_BUILD_BOARD := $(call readboardsparam,build.board)
BOARD_BUILD_IDE_VERSION := $(call readboardsparam,ide.version)
BOARD_BUILD_MCU := $(call readboardsparam,build.mcu)
BOARD_BUILD_FCPU := $(call readboardsparam,build.f_cpu)
BOARD_BUILD_VARIANT := $(call readboardsparam,build.variant)
BOARD_UPLOAD_SPEED := $(call readboardsparam,upload.speed)
BOARD_UPLOAD_PROTOCOL := $(call readboardsparam,upload.protocol)
BOARD_USB_VID := $(call readboardsparam,build.vid)
BOARD_USB_PID := $(call readboardsparam,build.pid)
BOARD_USB_PRODUCT := $(call readboardsparam,build.usb_product)
BOARD_USB_MANUFACTURER := $(call readboardsparam,build.usb_manufacturer)

# board compiler options
# BOARD_COMPILER_FLAGS += -D_FCPU=$(BOARD_BUILD_FCPU) -DARDUINO=$(BOARD_BUILD_IDE_VERSION) -DARDUINO_$(BOARD_BUILD_ARCH) -DARDUINO_$(BOARD_BUILD_BOARD)

# # USB definitions compiler options
# ifneq "$(BOARD_USB_VID)" ""
# USBFLAGS += -DUSB_VID=$(BOARD_USB_VID)
# endif
# ifneq "$(BOARD_USB_PID)" ""
# USBFLAGS += -DUSB_PID=$(BOARD_USB_PID)
# endif
# ifneq "$(BOARD_USB_PRODUCT)" ""
# USBFLAGS += -DUSB_PRODUCT=$(BOARD_USB_PRODUCT)
# endif
# ifneq "$(BOARD_USB_MANUFACTURER)" ""
# USBFLAGS += -DUSB_MANUFACTURER=$(BOARD_USB_MANUFACTURER)
# else
# USBFLAGS += -DUSB_MANUFACTURER="Unknown"
# endif

# targets
all: $(OUT)


## AVR
ifeq "$(BOARD_PLATFORM)" "avr"

# some avr specific configuration options
BOARD_BOOTLOADER_UNLOCK := $(call readboardsparam,bootloader.unlock_bits)
BOARD_BOOTLOADER_LOCK := $(call readboardsparam,bootloader.lock_bits)
BOARD_BOOTLOADER_LFUSES := $(call readboardsparam,bootloader.low_fuses)
BOARD_BOOTLOADER_HFUSES := $(call readboardsparam,bootloader.high_fuses)
BOARD_BOOTLOADER_EFUSES := $(call readboardsparam,bootloader.extended_fuses)
BOARD_BOOTLOADER_PATH := $(call readboardsparam,bootloader.path)
BOARD_BOOTLOADER_FILE := $(call readboardsparam,bootloader.file)

## avrdude configuration
ifeq "$(UPLOAD_TOOL_NAME)" "avrdude"

RUNTIME_AVRDUDE_PATH := $(shell echo "$(call readboardsparam,runtime.tools.avrdude.path)" | sed -e 's/\\/\//g')
ifneq "$(RUNTIME_AVRDUDE_PATH)" ""
AVRDUDE_PATH := $(RUNTIME_AVRDUDE_PATH)
else
AVRDUDE_PATH := $(call unixpath,'$(call readboardsparam,tools.avrdude.path)')
endif
AVRDUDE_CMD_PATH := $(call readboardsparam,tools.avrdude.cmd.path)
AVRDUDE ?= "$(shell echo "$(AVRDUDE_CMD_PATH)" | sed "s,{path},$(AVRDUDE_PATH),g")"
AVRDUDE_CFG_PATH := $(call readboardsparam,tools.avrdude.config.path)
AVRDUDECONF ?= "$(shell echo "$(AVRDUDE_CFG_PATH)" | sed "s,{path},$(AVRDUDE_PATH),g")"

# bootloader
BOOTLOADERHEX = "$(PLATFORMDIR)/bootloaders/$(BOARD_BOOTLOADER_PATH)/$(BOARD_BOOTLOADER_FILE)"

ifdef PROGRAMMER

programmerparam = $(shell sed -ne "s/^$(PROGRAMMER).$(1)=\(.*\)/\1/p" $(PROGRAMMERS))
PROGRAMMER_NAME := "$(call programmerparam,name)"
PROGRAMMER_COMMUNICATION := $(call programmerparam,communication)
PROGRAMMER_PROTOCOL := $(call programmerparam,protocol)
PROGRAMMER_SPEED := $(call programmerparam,speed)
PROGRAMMER_EXTRA_PARAMS := $(shell echo "$(call programmerparam,program.extra_params)" | sed "s/{serial.port}/$(SERIALDEV)/g" | sed "s/{program.speed}/$(PROGRAMMER_SPEED)/g" )
PROGRAMMER_CONFIG_PATH := $(shell echo "$(call programmerparam,config.path)" | sed "s,{runtime.platform.path},$(PLATFORMDIR),g")

# override config file
ifneq "$(PROGRAMMER_CONFIG_PATH)" ""
AVRDUDECONF = "$(PROGRAMMER_CONFIG_PATH)"
endif

endif

# do not erase flash and do not verify data
#AVRDUDEFLAGS += -DV
AVRDUDEFLAGS += -C "$(AVRDUDECONF)"
AVRDUDEFLAGS += -p $(BOARD_BUILD_MCU)

ifdef PROGRAMMER
# override protocol
ifneq "$(PROGRAMMER_PROTOCOL)" ""
AVRDUDEFLAGS +=  -c $(PROGRAMMER_PROTOCOL)
else
AVRDUDEFLAGS += -c $(BOARD_UPLOAD_PROTOCOL) 
endif

 # programmer overrides speed
ifeq "$(PROGRAMMER_SPEED)" ""
AVRDUDEFLAGS += -b $(BOARD_UPLOAD_SPEED)
endif

AVRDUDEFLAGS += $(PROGRAMMER_EXTRA_PARAMS)

else
AVRDUDEFLAGS += -c $(BOARD_UPLOAD_PROTOCOL) -b $(BOARD_UPLOAD_SPEED)
# device serial port
AVRDUDEFLAGS += -P $(SERIALDEV)
endif

ifeq ($(VERBOSE),1)
AVRDUDEFLAGS += -q
ARDUINO_BUILDER_ARGS += -verbose
else
AVRDUDEFLAGS += -q -q
endif

PHONY += upload bootloader

# programmer specific targets
upload: target
ifdef PROGRAMMER
	@echo "Uploading to $(BOARD_NAME) via $(PROGRAMMER_NAME)"
else
	@echo "Uploading to $(BOARD_NAME)"
endif
	$(AVRDUDE) $(AVRDUDEFLAGS) -U flash:w:"$(MAIN_INOFILE).hex":i

bootloader:
ifdef PROGRAMMER
	$(AVRDUDE) $(AVRDUDEFLAGS) -U lock:w:$(BOARD_BOOTLOADER_UNLOCK):m
	$(AVRDUDE) $(AVRDUDEFLAGS) -eU lfuse:w:$(BOARD_BOOTLOADER_LFUSES):m
	$(AVRDUDE) $(AVRDUDEFLAGS) -U hfuse:w:$(BOARD_BOOTLOADER_HFUSES):m
ifneq "$(BOARD_BOOTLOADER_EFUSES)" ""
	$(AVRDUDE) $(AVRDUDEFLAGS) -U efuse:w:$(BOARD_BOOTLOADER_EFUSES):m
endif
ifneq "$(BOOTLOADERHEX)" ""
	$(AVRDUDE) $(AVRDUDEFLAGS) -U flash:w:"$(BOOTLOADERHEX)":i
endif
	$(AVRDUDE) $(AVRDUDEFLAGS) -U lock:w:$(BOARD_BOOTLOADER_LOCK):m
else
	@echo "Uploading a bootloader is possible only using a programmer."
endif

## end of avrdude configuration
endif

## end of AVR configuration
endif

.PHONY:	all target size clean prefs clang-complete $(PHONY)

$(OUT): target

target:
	@$(ARDUINO_BUILDER) $(ARDUINO_BUILDER_ARGS) -compile $(MAIN_INOFILE)
	@for p in ${OUT} ; \
        do \
            mv $(OUTDIR)/$${p} . ; \
        done

size: target

prefs:
	@$(ARDUINO_BUILDER) $(ARDUINO_BUILDER_ARGS) -dump-prefs $(MAIN_INOFILE)

clean:
	@rm $(OUT)

clang-complete:
	@echo --target=$(BINUTILS_ARCH)
ifneq "$(BOARD_BUILD_MCU)" ""
	@echo -mmcu=$(BOARD_BUILD_MCU)
endif
	@echo -D_FCPU=$(BOARD_BUILD_FCPU)
	@echo -DARDUINO=$(BOARD_BUILD_IDE_VERSION)
	@echo -DARDUINO_$(BOARD_BUILD_ARCH)
	@echo -DARDUINO_$(BOARD_BUILD_BOARD)
# USB definitions compiler options
ifneq "$(BOARD_USB_VID)" ""
	@echo -DUSB_VID=$(BOARD_USB_VID)
endif
ifneq "$(BOARD_USB_PID)" ""
	@echo -DUSB_PID=$(BOARD_USB_PID)
endif
ifneq "$(BOARD_USB_PRODUCT)" ""
	@echo -DUSB_PRODUCT=$(BOARD_USB_PRODUCT)
endif
ifneq "$(BOARD_USB_MANUFACTURER)" ""
	@echo -DUSB_MANUFACTURER="\"$(BOARD_USB_MANUFACTURER)\""
else
	@echo -DUSB_MANUFACTURER="Unknown"
endif
ifneq "$(BINUTILS_INCLUDE)" ""
	@echo "-I$(BINUTILS_INCLUDE)"
	@test -d "$(BINUTILS_INCLUDE)/c++" && echo "-I$(BINUTILS_INCLUDE)/c++" || exit 0;
endif
	@echo "-I$(CORE_PATH)"
	@echo -e "-include\n$(CORE_PATH)/Arduino.h"
	@echo "-I$(VARIANT_PATH)"
ifneq "$(ARDUINO_LIBRARIES)" ""
	@for dir in "$(ARDUINO_LIBRARIES)"/*/; do \
         if [ -d "$${dir}" ] ; then \
             echo "-I$$dir" ;\
         fi;\
         if [ -d "$${dir}src" ] ; then \
            echo "-I$${dir}src/" ;\
         fi;\
     done
endif
ifneq "$(PLATFORMDIR)" ""
	@for dir in "$(PLATFORMDIR)"/libraries/*/; do \
        if [ -d "$${dir}" ] ; then \
             echo "-I$$dir" ;\
        fi;\
        if [ -d "$${dir}src" ] ; then \
           echo "-I$${dir}src/";\
        fi;\
    done
endif
ifneq "$(SKETCHBOOK_DIR)" ""
	@for dir in "$(SKETCHBOOK_DIR)"/libraries/*/; do \
        if [ -d "$${dir}" ] ; then \
             echo "-I$$dir" ;\
        fi;\
        if [ -d "$${dir}src" ] ; then \
           echo "-I$${dir}src/";\
        fi;\
    done
endif

# for debugging purposes
vars:
	@echo "ARDUINO_HARDWARE_TOOLS = $(ARDUINO_HARDWARE_TOOLS)"
	@echo "ARDUINO_LIBRARIES = $(ARDUINO_LIBRARIES)"
	@echo "ARDUINO_USERDIR = $(ARDUINO_USERDIR)"
	@echo "ARDUINO_USER_PACKAGES = $(ARDUINO_USER_PACKAGES)"
	@echo "SKETCHBOOK_DIR = $(SKETCHBOOK_DIR)"
	@echo "PREFERENCES_FILE = $(PREFERENCES_FILE)"
	@echo "PLATFORMDIR = $(PLATFORMDIR)"
	@echo "MAIN_INOFILE = $(MAIN_INOFILE)"
	@echo "OUTDIR = $(OUTDIR)"	
	@echo "UPLOAD_TOOL_NAME = $(UPLOAD_TOOL_NAME)"
	@echo "BINUTILS_ARCH = $(BINUTILS_ARCH)"
	@echo "BINUTILS_PATH = $(BINUTILS_PATH)"
	@echo "BINUTILS_INCLUDE = $(BINUTILS_INCLUDE)"
	@echo "BOARD = $(BOARD)"
	@echo "BOARD_NAME = $(BOARD_NAME)"
	@echo "BOARD_MANUFACTURER = $(BOARD_MANUFACTURER)"
	@echo "BOARD_PLATFORM = $(BOARD_PLATFORM)"
	@echo "BOARD_ID = $(BOARD_ID)"
	@echo "BOARD_BUILD_ARCH = $(BOARD_BUILD_ARCH)"
	@echo "BOARD_BUILD_BOARD = $(BOARD_BUILD_BOARD)"
	@echo "BOARD_BUILD_IDE_VERSION = $(BOARD_BUILD_IDE_VERSION)"
	@echo "BOARD_BUILD_MCU = $(BOARD_BUILD_MCU)"
	@echo "BOARD_BUILD_FCPU = $(BOARD_BUILD_FCPU)"
	@echo "BOARD_BUILD_VARIANT = $(BOARD_BUILD_VARIANT)"
	@echo "BOARD_UPLOAD_SPEED = $(BOARD_UPLOAD_SPEED)"
	@echo "BOARD_UPLOAD_PROTOCOL = $(BOARD_UPLOAD_PROTOCOL)"
	@echo "BOARD_USB_VID = $(BOARD_USB_VID)"
	@echo "BOARD_USB_PID = $(BOARD_USB_PID)"
	@echo "BOARD_USB_PRODUCT = $(BOARD_USB_PRODUCT)"
	@echo "BOARD_USB_MANUFACTURER = $(BOARD_USB_MANUFACTURER)"
ifeq "$(BOARD_PLATFORM)" "avr"
	@echo "AVRDUDE = $(AVRDUDE)"
	@echo "AVRDUDEFLAGS = $(AVRDUDEFLAGS)"
	@echo "BOARD_BOOTLOADER_UNLOCK = $(BOARD_BOOTLOADER_UNLOCK)"
	@echo "BOARD_BOOTLOADER_LOCK = $(BOARD_BOOTLOADER_LOCK)"
	@echo "BOARD_BOOTLOADER_LFUSES = $(BOARD_BOOTLOADER_LFUSES)"
	@echo "BOARD_BOOTLOADER_HFUSES = $(BOARD_BOOTLOADER_HFUSES)"
	@echo "BOARD_BOOTLOADER_EFUSES = $(BOARD_BOOTLOADER_EFUSES)"
	@echo "BOARD_BOOTLOADER_PATH = $(BOARD_BOOTLOADER_PATH)"
	@echo "BOARD_BOOTLOADER_FILE = $(BOARD_BOOTLOADER_FILE)"
endif
ifdef PROGRAMMER
	@echo "PROGRAMMER = $(PROGRAMMER)"
	@echo "PROGRAMMER_NAME = $(PROGRAMMER_NAME)"
	@echo "PROGRAMMER_COMMUNICATION = $(PROGRAMMER_COMMUNICATION)"
	@echo "PROGRAMMER_PROTOCOL = $(PROGRAMMER_PROTOCOL)"
	@echo "PROGRAMMER_SPEED = $(PROGRAMMER_SPEED)"
	@echo "PROGRAMMER_EXTRA_PARAMS = $(PROGRAMMER_EXTRA_PARAMS)"
	@echo "PROGRAMMER_CONFIG_PATH = $(PROGRAMMER_CONFIG_PATH)"
endif

