;; Emacs init file.
;;
;; By defaults loads main.el, which contains links
;; to the other parts of configuration.
;; You should better start from main.el and follow it.

;; (package-initialize) ; for compatibility with Emacs 25.

;; load file in emacs.d directory
(defun load-user-file (file-name)
  "Load file from emacs user directory."
  (let* ((path-no-ext (concat (file-name-as-directory user-emacs-directory)
							  file-name))
		 (path-source (concat path-no-ext ".el"))
		 (path-compiled (concat path-no-ext ".elc"))
		 (native-enabled (and (fboundp 'native-comp-available-p)
							  (native-comp-available-p))))
    (when (file-exists-p path-source)
	  ;; Disable native compilation of user files,
	  ;; as Emacs fails to load afterwards
	  (if (not native-enabled)
		  (progn
			(when (file-newer-than-file-p path-source path-compiled)
			  (byte-compile-file path-source))
			(load path-no-ext))
		(load path-source)))))

;; set default Emacs custom file
(setq custom-file (concat user-emacs-directory
                          "custom-emacs-"
                          (format "%d.%d" emacs-major-version emacs-minor-version)
                          ".el"))

;; compile and load Emacs configuration

(load-user-file "config-files/main")
(if (file-exists-p custom-file)
    (load custom-file)
  (let ((custom-files (directory-files
                       user-emacs-directory
                       nil
                       "^custom-emacs-.+\\..+\\.el$")))
    (dolist (cf custom-files)
      (load (concat user-emacs-directory cf))))) ; load custom file
(put 'downcase-region 'disabled nil)
